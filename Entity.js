process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();
require('dotenv').config()
const ChannelClass = require('App/Controllers/Common/Channel')
const Sentry = require('@sentry/node');

if (process.env.SPE_DSN) {
  Sentry.init({ dsn: process.env.SPE_DSN });
}
const channel = new ChannelClass();
channel.fromEnv();