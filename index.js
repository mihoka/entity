// みほか Mihoka
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();
require('dotenv').config()
const Master = require('./App/master');
const Sentry = require('@sentry/node');

if (process.env.SPE_DSN) {
  Sentry.init({ dsn: process.env.SPE_DSN });
}

console.log('*** Starting Master');
const self = new Master();
self.fetch();