const cp = require('child_process');
const tls = require('tls');
const ApiMihoka = require('./Controllers/Mihoka/Api');
const ApiTwitch = require('./Controllers/Twitch/Api');
const reportDiscord = require('./Helpers/DiscordReporter');

module.exports = class MasterHandler {
    constructor() {
        this.currentChannels = {};
        this.refresher = null;
        this.bearerToken = null;
        process.setMaxListeners(100);
    }
    createSocket() {
        this.sock = new tls.Socket();
        this.sock.setEncoding('utf8');
        this.sock.setKeepAlive(true, 3000);
        this.sock.setNoDelay();

        this.sock.on('error', (err) => {
            this.onError(err);
        });
        this.sock.on('close', (hadError) => {
            this.onClose(hadError);
        });
        this.sock.on('timeout', () => {
            this.onTimeout();
        });
        this.sock.on('end', () => {
            this.onEnd();
        })
        this.sock.on('data', (data) => {
            this.onData(data);
        });
        this.sock.on('connect', () => {
            this.onConnect();
        });
    }
    finish() {
        // finishes the socket connection
        this.sock.end();
        this.sock.destroy();
    }
    connect() {
        // Connects to the master
    }
    reconnect() {
        // reconnects to the master
        reportDiscord('RCON needed', new Error('n/a'))
        this.finish();
        this.createSocket();
        this.connect();
    }

    // Old api version
    fetch () {
        const that = this;
        ApiTwitch.getBearerId().then((res) => {
            that.bearerToken = res.body.access_token;
        }).
        then(() => {
            // fetches all channels data to start
            ApiMihoka.getChannels().then((channels) => {
                channels.results.forEach((channel) => {
                    that.spawn(channel);
                });
            }).
            catch((err) => console.error(JSON.stringify(err), 'Failed to fetch channels list.'));
        }).
        catch((err) => {
            console.error('Failed to get bearer token, exiting:', err);
            // eslint-disable-next-line no-process-exit
            process.exit(1);
        })
        this.refresher = setInterval(() => {
            that.refresh();
        }, 120*1000);
    }
    // Old api version
    refresh () {
        const that = this;
        ApiTwitch.getBearerId().then((res) => {
            that.bearerToken = res.body.access_token;
        }).
        catch(console.error);
        // Starts all channels that are not yet started
        ApiMihoka.getChannels().then((channels) => {
            channels.results.forEach(async (channel) => {
                if (!Reflect.has(this.currentChannels, channel.uuid)) {
                    that.spawn(channel);
                    await (new Promise(res => setTimeout(res, 1500)));
                }
            });
        }).
        catch(console.error);
    }

    spawn (cha) {
        if (!cha.uuid) {
            return;
        }
        const env = {
            CHANNEL_ID: cha.uuid,
            ROOM_ID: cha.roomId,
            ROOM_NAME: cha.roomName,
            NODE_PATH: './',
            FORCE_COLOR: true,
            'TCH_TOK': this.bearerToken,
        };
        [
            'TCH_USERNAME',
            'TCH_PASSWORD',
            'TCH_HOSTNAME',
            'TCH_PORT',
            'TCH_API',
            'TCH_CID',
            'TCH_CSK',
            'SPE_API',
            'SPE_TOK',
            'SPE_DSN',
            'SPE_API_HOST',
            'SPE_API_PORT',
            'APP_DEBUG',
            'APP_ENV',
        ].forEach((key) => Reflect.set(env, key, Reflect.get(process.env, key)));
        try {
            const channelProcess = cp.fork('./Entity.js', {
                detached: false,
                env,
            });
            channelProcess.setMaxListeners(100);
            this.currentChannels[cha.uuid] = {
                process: channelProcess,
            }
            channelProcess.addListener('exit', (code, sig) => {
                console.log('*** Channel process exited:', cha.roomName, cha.uuid, code, sig);
                setTimeout(() => {
                    console.log('*** Restarting channel', cha.roomName, cha.uuid);
                    this.spawn(cha);
                }, 1500);
            });
            process.addListener('beforeExit', (_, sig) => {
                this.currentChannels[cha.uuid].process.kill(sig);
            });
        } catch (e) {
            console.error('oh no', JSON.stringify(e));
        }
    }
}
