require('dotenv').config();
module.exports = function Env(key) {
    return Reflect.get(process.env, key) || null;
}