module.exports = class IrcHelper {
    static isPing(raw) {
        return (/^PING :(.*)/gi).test(raw);
    }
    static isPong(raw) {
        return (/^PONG :(.*)/gi).test(raw);
    }
    static hasTags(raw) {
        return (/^@(.*)/gi).test(raw);
    }
    static isTags(raw) {
        return (/^([\S]=[^\S];)+/gi).test(raw);
    }
    static hasColon(raw) {
        return (/^:(.*)/gi).test(raw);
    }
    static messageRegex() {
        return (/^(@?\S+)?\s?:(\S+)\s(\S+\s\*\s(\S+)|\S+)\s:?(\S+)?\s?=?\s?(#\S+)?\s?:?(.*)?/i);
    }
    static parse(raw) {
        return this.messageRegex().exec(raw);
    }
    static getUsername(raw) {
        return (/(\S+)!.*/gi).exec(raw);
    }
    static sanitizeText(message) {
        let sanitizedMessage = message;
        // Looking for commands
        const regexPattern = /^(\.|\/)\S+/i;
        sanitizedMessage = sanitizedMessage.trim();
        const isFound = regexPattern.exec(sanitizedMessage);
        if (isFound) {
            // A regex has been found here
            sanitizedMessage = sanitizedMessage.substring(isFound[0].length);
            sanitizedMessage = IrcHelper.sanitizeText(sanitizedMessage.trim());
        }
        return sanitizedMessage;
    }
    static parseTags(raw) {
        const tags = {
            count: 0,
        };
        const rx = (/@?(([^;=]+)=([^;\s]+))/g);
        let found = null;
        do {
            found = rx.exec(raw);
            if (found !== null) {
                tags[found[2]] = found[3];
                tags.count += 1;
            }
        } while (found !== null)
        return tags;
    }
    static parseBadges(raw) {
        const badges = {
        };
        const regx = (/([a-zA-Z0-9-_]+)\/(\d+),?/g);
        let found = null;
        do {
            found = regx.exec(raw);
            if (found !== null) {
                badges[found[1]] = found[2];
            }
        } while (found !== null);
        return badges;
    }
}