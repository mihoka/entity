/*
  Twitch rates

  [Messages]
  Limit                 Applies to ...
  20 per 30 seconds     Users sending commands or messages to channels in which they do not have Moderator or Operator status
  100 per 30 seconds    Users sending commands or messages to channels in which they have Moderator or Operator status
  50 per 30 seconds     Known bots
  7500 per 30 seconds   Verified bots

  [Whispers]
  Users (not bots)
  => 3 per second, up to 100 per minute; 40 accounts per day; 160 recipients per day
  Known bots (The recipients limit applies to known bots sharing an IP address.)
  =>  10 per second, up to 200 per minute; 500 accounts per day; 1000 recipients per day

  Verified bots
  => 20 per second, up to 1200 per minute; 100,000 accounts per day; 100,000 recipients per day
*/

module.exports = class RateLimiter {
  constructor(messageCount, messageDelay, whisperCount, whisperDelay) {
    this.messagesQuota = messageCount;
    this.messagesDelay = messageDelay;
    this.whispersQuota = whisperCount;
    this.whispersDelay = whisperDelay;
  }

  set messagesQuota (val) {
    if (!Number.isSafeInteger(val)) {
      return;
    }
    this._messagesQuota = val;
  }
  set whispersQuota (val) {
    if (!Number.isSafeInteger(val)) {
      return;
    }
    this._whispersQuota = val;
  }
  get messagesQuota () {
    return typeof this._messagesQuota === 'undefined' ? 50 : this._messagesQuota;
  }
  get whispersQuota () {
    return typeof this._whispersQuota === 'undefined' ? 100 : this._whispersQuota;
  }

  set messagesDelay (val) {
    if (!Number.isSafeInteger(val)) {
      return;
    }
    this._messagesQuota = val;
  }
  set whispersDelay (val) {
    if (!Number.isSafeInteger(val)) {
      return;
    }
    this._whispersDelay = val;
  }
  get messagesDelay () {
    return typeof this._messagesDelay === 'undefined' ? 50 : this._messagesDelay;
  }
  get whispersDelay () {
    return typeof this._whispersDelay === 'undefined' ? 100 : this._whispersDelay;
  }
}