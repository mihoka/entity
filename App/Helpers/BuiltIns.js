/* eslint-disable max-lines */
/* eslint-disable no-invalid-this */
const ApiTwitch = require('App/Controllers/Twitch/Api');
const Command = require('App/Classes/Command');
const Giveaway = require('App/Classes/Giveaway');
const AppInformations = require('App/Config/App');
const uuid = require('uuid/v4');
const superagent = require('superagent');

class BuiltIns {
    static commands () {
        return [
            BuiltIns.giveaway(),
            BuiltIns.uptime(),
            BuiltIns.raffle(),
            BuiltIns.commandsManagement(),
            BuiltIns.welcomes(),
            BuiltIns.fc(),
            BuiltIns.mihoka(),
            BuiltIns.shoutout(),
            BuiltIns.vips(),
            BuiltIns.roll(),
            BuiltIns.quote(),
            BuiltIns.automessage(),
            BuiltIns.waveBack(),
            BuiltIns.average(),
            BuiltIns.customize(),
        ];
    }
    static uptime() {
        const uptime = new Command().
        setDelay(300).
        setRegex(/^!uptime(?:$|\s)/i).
        setHandler(function handler() {
            const _apiResult = ApiTwitch.getCurrentStream({ id: this.channel._id });
            _apiResult.then((data) => {
                if (data.found) {
                    const _startedAt = new Date(data.result.started_at);
                    const _now = new Date();
                    const _diff = _now - _startedAt;
                    const _secs = Math.floor(_diff % 60000 / 1000);
                    const _mins = Math.floor(_diff / 60000) % 60;
                    const _hours = Math.floor(_diff / 3600000) % 24;
                    const _days = Math.floor(_diff / 86400000);
                    let _str = '';
                    if (_days > 0) {
                        _str += _days + ' jours, ';
                    }
                    if (_hours > 0) {
                        _str += _hours + ' heures, ';
                    }
                    if (_mins > 0) {
                        _str += _mins + ' minutes';
                    }
                    if (_secs > 0) {
                        _str += (_str.length > 0 ? ', ' : '') + _secs + ' secondes';
                    }
                    if (_str.length > 0) {
                        this.channel.enqueue(`Le stream est actif depuis ${_str}.`);
                    }
                }
            }).catch((err) => console.error(err));
        });
        return uptime;
    }
    static average() {
        const avgMain = new Command().
        setIgnoreDelay(true).
        setRegex(/^!avg/i).
        setHandler(function handler(message) {
            if (!message.user.moderator && !message.user.creator && !message.user.broadcaster) {
                return false;
            }
            return true;
        });
        const avgStart = new Command().
        setDelay(10).
        setRegex(/^!avg on/i).
        setHandler(function handler(message) {
            if (!message.user.moderator && !message.user.creator && !message.user.broadcaster) {
                return;
            }
            this.channel.createAverageNode();
            this.channel._settings.average.data = {};
            this.channel._settings.average.recording = true;
            this.channel.enqueue(`La notation est désormais activée ! Écrivez une note entre 0 et 10 (inclus) pour participer.`);
        });
        const avgStop = new Command().
        setDelay(10).
        setRegex(/^!avg off/i).
        setHandler(function handler(message) {
            if (!message.user.moderator && !message.user.creator && !message.user.broadcaster) {
                return false;
            }
            this.channel.createAverageNode();
            if (!this.channel._settings.average.recording) {
                return false;
            }
            this.channel._settings.average.recording = false;
            const keys = Object.keys(this.channel._settings.average.data);
            const count = keys.length;
            if (count === 0) {
                return false;
            }
            let sum = 0;
            keys.forEach((key) => {
                sum += this.channel._settings.average.data[key];
            });
            const avg = sum / count;
            this.channel.enqueue(`Fin du calcul de notations ! La moyenne est de ${avg} !`);
            return true;
        });
        avgMain.addAll([avgStart, avgStop]);
        return avgMain;
    }
    static waveBack () {
        const waveBack = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:bonjour|salut|hey|Oi|Hei|Hej|coucou|bonsoir|yo) (\S+)/i).
            setHandler(function handler(message) {
                const wavebackPool = ['Salut %s !', 'Coucou %s :)', 'Hey %s ;)', '... coucou %s.', '%s ? Je... *rougis* s-salut'];
                const fetchedName = this.command.regex.exec(message.body);
                if (fetchedName && fetchedName.length > 1 && String(fetchedName[1]).toLowerCase() === this.channel._ircUser.display.toLowerCase()) {
                    // randomly get a answer
                    const maxCountValue = wavebackPool.length;
                    const randomizedValue = Math.floor(Math.random()*maxCountValue);
                    this.channel.enqueue(String(wavebackPool[randomizedValue]).replace('%s', message.user.display));
                    return true;
                }
                return false;
            });
        return waveBack;
    }
    static giveaway () {
        // $giveaway
        const ga = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:(?:\$|!)giveaway|!ga) (draw|start|end|status)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.enqueue('Sous-commandes disponibles: !giveaway start/draw/end/stop');
                return true;
        });
        // $giveaway draw
        const giveawayDraw = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:(?:\$|!)giveaway|!ga) draw(?:\s(\d+))?/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildGiveawayNode();
                if (!this.channel._giveaways.current) {
                    this.channel.enqueue(`Il n'y a aucun giveaway en cours actuellement.`);
                    return false;
                }
                // Drawing a winner
                const drawResult = this.channel._giveaways.current.draw(this.command.regex.exec(message.body)[1]);
                this.channel.triggerUpdate();
                if (drawResult.total === 0) {
                    this.channel.enqueue(`Il n'y a plus aucun membre non gagnant dans le tirage en cours. Fin du giveaway !`);
                    this.channel._giveaways.previous.push(this.channel._giveaways.current);
                    this.channel._giveaways.current = null;
                    return false;
                }

                // Follow verification async, and answer to channel
                drawResult.drawn.forEach((winner) => {
                    if (winner.id === this.channel._id) {
                        this.channel.enqueue(`Le/la gagnant(e) tiré(e) est ${winner.display}, et est le propriétaire de la chaîne!`);
                        return;
                    }
                    ApiTwitch.isFollowing(winner.id, this.channel._id).then((apiResult) => {
                        // User follows or not, and we get also the follow date with it
                        if (apiResult.follows) {
                            const sinceDate = new Date(apiResult.since);
                            const daySinceFollow = Math.floor((Date.now() - new Date(apiResult.since)) / (1000*60*60*24));
                            this.channel.enqueue(`Le/la gagnant(e) tiré(e) est ${winner.display}; Il/elle suit la chaine depuis le ${sinceDate.getUTCFullYear()}/${String(sinceDate.getUTCMonth()+1).padStart(2, '0')}/${String(sinceDate.getUTCDate()).padStart(2,'0')} ${String(sinceDate.getUTCHours()).padStart(2, '0')}H${String(sinceDate.getUTCMinutes()).padStart(2, '0')}. (${daySinceFollow} jours)`);
                        } else {
                            this.channel.enqueue(`Le/la gagnant(e) tiré(e) est ${winner.display}; Il/elle ne suit pas la chaine`);
                        }
                    }).
                    catch((error) => {
                        const _errMessage = 'BuiltIns::giveaway()$giveaway_draw:apiTwitch.isFollowing, unable to fetch the results from APIs.';
                        console.error(_errMessage, error);
                        this.channel.enqueue(`Le/la gagnant(e) tiré(e) est ${winner.display}; Il est impossible de vérifier si il/elle suit la chaine dû à un problème.`);
                    });
                });
                return true;
        });
        // $giveaway start
        const giveawayStart = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:(?:\$|!)giveaway|!ga) start/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildGiveawayNode();
                if (this.channel._giveaways.current) {
                    if (this.channel._giveaways.current.isRunning()) {
                        this.channel.enqueue(`Impossible de lancer un nouveau giveaway: Il y a déjà un giveaway en cours (et aucun gagnant tiré) actuellement.`);
                        return false;
                    }
                    // Moving any existing current giveaway to the previous list
                    this.channel._giveaways.previous.push(this.channel._giveaways.current);
                    this.channel._giveaways.current = null;
                }
                this.channel._giveaways.current = new Giveaway();
                this.channel.enqueue(`Le giveaway est désormais lancé ! N'hésitez pas à taper !raffle pour participer !`);
                this.channel.triggerUpdate();
                return true;
        });
        // $giveaway end
        const giveawayEnd = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:(?:\$|!)giveaway|!ga) (?:end|close)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildGiveawayNode();
                if (!this.channel._giveaways.current || !this.channel._giveaways.current.isRunning()) {
                    // There is no giveaway running
                    this.channel.enqueue(`Il n'y a aucun giveaway en cours actuellement.`);
                    return false;
                }
                // Giveaway is in progress, we can stop it
                this.channel.enqueue(`Le giveaway est désormais fermé. Aucune nouvelle participation ne sera acceptée.`);
                this.channel._giveaways.current.close();
                this.channel.triggerUpdate();
                return true;
        });

        // $giveaway status
        const giveawayInfo = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:(?:\$|!)giveaway|!ga) status/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildGiveawayNode();
                if (!this.channel._giveaways.current) {
                    // There is no giveaway running
                    this.channel.enqueue(`Il n'y a aucun giveaway en cours actuellement.`);
                    return false;
                }
                // Giveaway is in progress, we can stop it
                const cga = this.channel._giveaways.current;
                const crea = new Date(cga.createdAt);
                this.channel.enqueue(`Giveaway démarré le ${crea.getUTCFullYear()}/${String(crea.getUTCMonth()+1).padStart(2, '0')}/${String(crea.getUTCDate()).padStart(2, '0')}, ${String(crea.getUTCHours()).padStart(2, '0')}:${String(crea.getUTCMinutes()).padStart(2, '0')}:${String(crea.getUTCSeconds()).padStart(2, '0')} GMT. Il y a ${cga.members.length} participants inscrits. Etat actuel du giveaway: ${cga.statusName}`);
                return true;
        });
        // Adding sub-commands
        ga.add(giveawayEnd);
        ga.add(giveawayDraw);
        ga.add(giveawayStart);
        ga.add(giveawayInfo);
        return ga;
    }
    static raffle () {
        const gaRaffle = new Command().
            setIgnoreDelay(true).
            setRegex(/^!raffle/i).
            setHandler(function handler(message) {
                this.channel.buildGiveawayNode();
                if (!this.channel._giveaways.current) {
                    return false;
                }
                if (!this.channel._giveaways.current.isRunning()) {
                    // The giveaway is not open to registrations
                    return false;
                }
                if (this.channel._giveaways.current.hasMember(message.user)) {
                    // Checking if the user is in the array already
                    return false;
                }
                this.channel._giveaways.current.add(message.user);
                this.channel.enqueue(`Merci ${message.user.display}, vous êtes désormais inscrit(e) au giveaway! VoHiYo`);
                this.channel.triggerUpdate();
                return true;
        });
        return gaRaffle;
    }
    static vips () {
        const vip = new Command().
            setIgnoreDelay(true).
            setRegex(/^!vip (add|remove|del|message|msg)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                this.channel.enqueue(`Sous-commandes disponibles pour !vip: 'add %s' (ajoute %s à la liste des VIPs), 'remove %s' (retires %s de la liste des VIPs), 'message %s' (définis %s comme message de bienvenue pour les vips).`);
                return true;
        });
        const vipAdd = new Command().
            setIgnoreDelay(true).
            setRegex(/^!vip add ([a-zA-Z0-9_]+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                const soTarget = regexParse[1].toLowerCase();
                const userInformations = ApiTwitch.getUser({ login: soTarget });
                userInformations.then((result) => {
                    if (result.found) {
                        // The user is found
                        const userDisplay = result.result.display_name;
                        this.channel._settings.vip.list.push(result.result.id);
                        this.channel.enqueue(`${message.user.display}, l'utilisateur ${userDisplay} a été ajouté à la liste des VIPs.`);
                    } else {
                        // Unable to find user with that login
                        this.channel.enqueue(`Erm... Je n'ai pas trouvé d'informations sur cette personne :/ Vous êtes sûr qu'il s'agit de son nom de connexion? (exemple: 'belette', pas 'イタチ')`);
                    }
                }).catch((reason) => {
                    // Error while fetching the data
                    const _errMessage = 'Error while fetching informations from Twitch API: ';
                    console.error(_errMessage, reason);
                    this.channel.enqueue(`Il semblerait qu'une erreur se soit produite durant le traitement de cette demande. Réessayez plus tard, peut-être? VoHiYo`);
                });
                this.channel.triggerUpdate();
                return true;
        });
        const vipDel = new Command().
            setIgnoreDelay(true).
            setRegex(/^!vip (?:remove|del) ([a-zA-Z0-9_]+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                const soTarget = regexParse[1].toLowerCase();
                const userInformations = ApiTwitch.getUser({ login: soTarget });
                userInformations.then((result) => {
                    if (result.found) {
                        // The user is found
                        const userDisplay = result.result.display_name;
                        const indexOfVIP = this.channel._settings.vip.list.indexOf(result.result.id);
                        if (indexOfVIP === -1) {
                            this.channel.enqueue(`${message.user.display}, l'utilisateur ${userDisplay} n'est pas dans la liste des VIPs.`);
                        } else {
                            this.channel._settings.vip.list.splice(indexOfVIP, 1);
                            this.channel.enqueue(`${message.user.display}, l'utilisateur ${userDisplay} a été retiré de la liste des VIPs.`);
                        }
                    } else {
                        // Unable to find user with that login
                        this.channel.enqueue(`Erm... Je n'ai pas trouvé d'informations sur cette personne :/ Vous êtes sûr qu'il s'agit de son nom de connexion? (exemple: 'belette', pas 'イタチ')`);
                    }
                }).catch((reason) => {
                    // Error while fetching the data
                    const _errMessage = 'Error while fetching informations from Twitch API: ';
                    console.error(_errMessage, reason);
                    this.channel.enqueue(`Il semblerait qu'une erreur se soit produite durant le traitement de cette demande. Réessayez plus tard, peut-être? VoHiYo`);
                });
                this.channel.triggerUpdate();
                return true;
        });
        const vipMsg = new Command().
            setIgnoreDelay(true).
            setRegex(/^!vip (?:msg|message) (.*)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                const thevipMsg = regexParse[1];
                if (!thevipMsg || thevipMsg.length < 3) {
                    this.channel.enqueue(`Un message est requis pour cette commande. Format: !vip message <message>; mettre %s permet d'insérer le nom du VIP recevant le message de bienvenue.`);
                    return false;
                }
                this.channel._settings.vip.message = thevipMsg;
                this.channel.enqueue(`${message.user.display}, le message de bienvenue des VIPs a été mis à jour.`);
                this.channel.triggerUpdate();
                return true;
        });
        vip.add(vipAdd);
        vip.add(vipDel);
        vip.add(vipMsg);
        return vip;
    }
    static automessage () {
        const automessage = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am)(?:\s|$)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                this.channel.enqueue(`!automessage / !am - permet de gérer les messages automatiques. Sous commandes: !am add (ajouter un message) !am remove (permet de supprimer un automessage) !am mode (permet de changer le type d'automessage) !am set:minutes <mins> (set the delay in minutes between two messages) !am set:messages <messages> (sets the delay in message count between two messages) !am on/off/1/0/enable/disable (enables or disables message).`);
                return true;
            });
        const automessageSetMessages = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) set:messages (\d+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length !== 2) {
                    this.channel.enqueue(`@${message.user.display}, une erreur est arrivée lors de la tentative d'analyse du message. Réessayez plus tard VoHiYo`);
                    return false;
                }
                const messagesDelay = parseInt(regexParse[1],10);
                if (isNaN(messagesDelay) || messagesDelay < 1) {
                    this.channel.enqueue(`@${message.user.display}, désolée mais vous devez fournir un nombre de messages supérieur à zéro.`);
                    return false;
                }

                this.channel._settings.announces.rules.messages = messagesDelay;
                this.channel.enqueue(`La règle d'AutoMessage a été mise à jour. Le nombre de messages requis entre deux annonces est de désormais de ${messagesDelay}.`);
                this.channel.triggerUpdate();
                return true;
            });
        const automessageSetMinutes = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) set:minutes (\d+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length !== 2) {
                    this.channel.enqueue(`@${message.user.display}, une erreur est arrivée lors de la tentative d'analyse du message. Réessayez plus tard VoHiYo`);
                    return false;
                }
                const minutesDelay = parseInt(regexParse[1],10);
                if (isNaN(minutesDelay) || minutesDelay < 1) {
                    this.channel.enqueue(`@${message.user.display}, désolée mais vous devez fournir un délai supérieur à zéro.`);
                    return false;
                }

                this.channel._settings.announces.rules.minutes = minutesDelay;
                this.channel.enqueue(`La règle d'AutoMessage a été mise à jour. Le délai minimal entre deux annonces est de désormais de ${minutesDelay} minutes.`);
                this.channel.triggerUpdate();
                return true;
            });
        const automessageAdd = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) add (.*)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const autoMsg = regexParsed[1];
                this.channel._settings.announces.list.push(autoMsg);
                this.channel.enqueue(`Le message automatique a été ajouté avec succès! (ID #${this.channel._settings.announces.list.length}) VoHiYo`);
                this.channel.triggerUpdate();
                return true;
            });
        const automessageSearch = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) search (.*)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const searchString = regexParsed[1];
                const matchedAnnounces = [];
                this.channel._settings.announces.list.forEach((elem, index) => {
                    if (elem.toLowerCase().indexOf(searchString.toLowerCase()) !== -1) {
                        matchedAnnounces.push(index+1);
                    }
                });
                if (matchedAnnounces.length === 0) {
                    this.channel.enqueue(`Aucun AutoMessage ne contient le texte entré dans la recherche.`);
                } else {
                    let matchedString = '';
                    matchedAnnounces.forEach((elem, index) => {
                        matchedString += `#${elem}${matchedAnnounces.length -1 === index ? '':', '}`;
                    });
                    this.channel.enqueue(`Le texte recherché apparait dans les AutoMessages suivants: ${matchedString}`);
                }
                return true;
            });
        const automessageDel = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) (?:del|remove|rm|delete) (\d+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const msgId = parseInt(regexParsed[1],10);
                if (isNaN(msgId)) {
                    this.channel.enqueue(`Veuillez spécifier un identifiant numérique uniquement.`);
                    return false;
                }
                if (this.channel._settings.announces.list.length === 0) {
                    this.channel.enqueue(`Désolée mais il n'y a pas de messages automatiques définis. Ajoutez-en un avant de le supprimer ! VoHiYo`);
                    return false;
                }

                if (this.channel._settings.announces.list.length < msgId) {
                    this.channel.enqueue(`Désolée mais aucun message automatique ne possède cette ID. Notez que si vous avez supprimé un automessage, son ID descends de 1 (ID 9 devient 8 si un ID inférieur a 8 a été supprimé, par example).`);
                    return false;
                }
                const removed = this.channel._settings.announces.list.splice(msgId -1, 1);
                if (removed.length !== 1) {
                    this.channel.enqueue(`Woops... Il semblerait que je n'ai pas pu retirer quoi que ce soit... Désolée :( `);
                    return false;
                }
                this.channel.enqueue(`Le message automatique a été supprimé avec succès! VoHiYo`);
                this.channel.triggerUpdate();
                return true;
            });
            const automessageUpdate = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) (?:update|edit|up) (\d+) (.*)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const msgId = parseInt(regexParsed[1],10);
                const msgText = regexParsed[2];
                if (isNaN(msgId)) {
                    this.channel.enqueue(`Veuillez spécifier un identifiant numérique uniquement.`);
                    return false;
                }
                if (this.channel._settings.announces.list.length === 0) {
                    this.channel.enqueue(`Désolée mais il n'y a pas de messages automatiques définis. Ajoutez-en un avant de l'éditer ! VoHiYo`);
                    return false;
                }

                if (this.channel._settings.announces.list.length < msgId) {
                    this.channel.enqueue(`Désolée mais aucun message automatique ne possède cette ID. Notez que si vous avez supprimé un automessage, son ID descends de 1 (ID 9 devient 8 si un ID inférieur a 8 a été supprimé, par example).`);
                    return false;
                }
                const removed = this.channel._settings.announces.list.splice(msgId -1, 1, msgText);
                if (removed.length !== 1) {
                    this.channel.enqueue(`Woops... Il semblerait que je n'ai pas pu remplacer quoi que ce soit... Désolée :( `);
                    return false;
                }
                this.channel.enqueue(`Le message automatique a été édité avec succès! VoHiYo`);
                this.channel.triggerUpdate();
                return true;
            });
        const automessageInfo = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) (?:show|info|see|view) (\d+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const msgId = parseInt(regexParsed[1],10);
                if (isNaN(msgId)) {
                    this.channel.enqueue(`Veuillez spécifier un identifiant numérique uniquement.`);
                    return false;
                }
                if (this.channel._settings.announces.list.length === 0) {
                    this.channel.enqueue(`Désolée mais il n'y a pas de messages automatiques définis ! VoHiYo`);
                    return false;
                }

                if (this.channel._settings.announces.list.length < msgId) {
                    this.channel.enqueue(`Désolée mais aucun message automatique ne possède cette ID. Notez que si vous avez supprimé un automessage, son ID descends de 1 (ID 9 devient 8 si un ID inférieur a 8 a été supprimé, par example).`);
                    return false;
                }
                const messageRetrieved = this.channel._settings.announces.list[msgId - 1];
                this.channel.enqueue(`Le message automatique #${msgId} est le suivant: ${messageRetrieved}`);
                this.channel.triggerUpdate();
                return true;
            });
        const automessageMode = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) mode (\d)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const obtainedIndex = parseInt(regexParsed[1],10) || null;
                const validNumbers = [1, 2, 3];
                if (isNaN(obtainedIndex) || validNumbers.indexOf(obtainedIndex) === -1) {
                    this.channel.enqueue(`Désolée mais le mode entré n'est pas valide. Il doit être une addition des modes suivants: [1] Envoyer toutes les X minutes [2] Uniquement si X messages ont été écrits`);
                    return false;
                }
                this.channel._settings.announces.mode = obtainedIndex;
                const labelsIndex = {
                    '1': `Envoyer un message toute les ${this.channel._settings.announces.rules.minutes} minutes`,
                    '2': `Envoyer un message tout les ${this.channel._settings.announces.rules.messages} messages`,
                    '3': `Envoyer un message toute les ${this.channel._settings.announces.rules.minutes} minutes, uniquement si ${this.channel._settings.announces.rules.messages} messages ont été postés.`,
                };
                this.channel.enqueue(`La configuration des AutoMessages a été mise à jour ! Nouvelle règle d'activation: ${Reflect.get(labelsIndex, this.channel._settings.announces.mode)}`);
                this.channel.triggerUpdate();
                return true;
            });
        const automessageEnableDisable = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!automessage|!am) (1|0|on|off|enable|disable)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParsed = this.command.regex.exec(message.body);
                const mode = regexParsed[1];
                if (['1','on','enable'].indexOf(mode)>-1) {
                    // enabling the autoMessage feature
                    this.channel._settings.announces.enabled = true;
                } else {
                    // disablingthe automessage feature
                    this.channel._settings.announces.enabled = false;
                }
                this.channel.enqueue(`Les AutoMessages sont désormais ${this.channel._settings.announces.enabled ? 'activés' : 'désactivés'}! VoHiYo`);
                this.channel.triggerUpdate();
                return true;
            });
        automessage.addAll([
            automessageAdd,
            automessageDel,
            automessageMode,
            automessageEnableDisable,
            automessageSetMinutes,
            automessageSetMessages,
            automessageInfo,
            automessageSearch,
            automessageUpdate,
        ]);
        return automessage;
    }
    static quote () {
        const quote = new Command().
            setRegex(/^!quote\s?(\d+)?/i).
            setDelay(10).
            setHandler(function handler(message) {
                this.channel.buildQuoteNode();
                if (this.channel._settings.quotes.length === 0) {
                    // no quotes found
                    this.channel.enqueue(`Aucune quote n'est présente dans les registres. Demandez à un modérateur d'en ajouter une ! VoHiYo`);
                    return false;
                }
                // Fully random
                const regexParse = this.command.regex.exec(message.body);
                let lowestIndex = Math.floor(Math.random()*this.channel._settings.quotes.length);
                if (regexParse && regexParse.length > 0) {
                    const tmpId = parseInt(regexParse[1],10) || -1;
                    if (tmpId-1 > -1 && tmpId-1 < this.channel._settings.quotes.length) {
                        lowestIndex = tmpId - 1;
                    }
                }
                const lowestCalled = this.channel._settings.quotes[lowestIndex];
                const quoteDate = new Date(lowestCalled.createdAt);
                const gameInfo = lowestCalled.game ? `[🎮 ${lowestCalled.game}]` : '';
                // common logic
                lowestCalled.displays += 1;
                this.channel._settings.quotes.splice(lowestIndex, 1, lowestCalled);
                this.channel.enqueue(`[#${lowestIndex+1}] "${lowestCalled.body}" - par ${lowestCalled.creator.display}, ${quoteDate.getUTCFullYear()}/${String(quoteDate.getUTCMonth()+1).padStart(2, '0')}/${String(quoteDate.getUTCDate()).padStart(2, '0')} à ${String(quoteDate.getUTCHours()).padStart(2, '0')}H${String(quoteDate.getUTCMinutes()).padStart(2, '0')} GMT ${gameInfo}`);
                this.channel.triggerUpdate();
                return true;
            });
        const quoteAdd = new Command().
            setRegex(/^!quote add (.*)/i).
            setDelay(3).
            setHandler(function handler(message) {
                this.channel.buildQuoteNode();
                // check mod status
                // if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                //     this.message.enqueue(`Désolée ${message.user.display} mais seuls les modérateurs et le Broadcaster peuvent utiliser cette commande VoHiYo`);
                //     return false;
                // }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length === 0) {
                    return false;
                }
                const quoteBody = regexParse[1];
                if (quoteBody.length < 3) {
                    this.channel.enqueue(`Impossible d'ajouter une quote si elle fais moins de 3 caractères de long!`);
                }
                const quoteObject = {
                    creator: message.user,
                    displays: 0,
                    game: null,
                    createdAt: new Date(),
                    body: quoteBody,
                };
                const _push = () => {
                    const quoteIndex = this.channel._settings.quotes.push(quoteObject);
                    this.channel.enqueue(`${message.user.display}, la quote a été ajoutée! Son identifiant est #${quoteIndex}: "${quoteObject.body}"${quoteObject.game ? ', Jeu: ' + quoteObject.game : ''}`);
                    this.channel.triggerUpdate();
                }
                // calling to get game
                const _getStream = ApiTwitch.getCurrentStream({
                    id: this.channel._id,
                    login: this.channel._name,
                });
                _getStream.then((result) => {
                    if (result.found) {
                        const _getGame = ApiTwitch.getGame(result.result.game_id);
                        _getGame.then((resultGame) => {
                            if (resultGame.found) {
                                quoteObject.game = resultGame.result.name;
                            }
                            _push();
                            return true;
                        }).catch((e2) => {
                            const _errMessage = 'Error while fetching the Game informations: ';
                            console.error(_errMessage, e2);
                            _push();
                            return true;
                        });
                    } else {
                        _push();
                    }
                    return true;
                }).catch((e1) => {
                    const _errMessage = 'Error while fetching the Stream informations:';
                    console.error(_errMessage, e1);
                    _push();
                    return true;
                });
                // end of game fetch
                // const quoteIndex = this.channel._settings.quotes.push(quoteObject);
                // this.channel.enqueue(`${message.user.display}, la quote a été ajoutée! Son identifiant est #${quoteIndex}`);
                // this.channel.triggerUpdate();
                return true;
            });
        const quoteDel = new Command().
            setRegex(/^!quote (?:del|delete|remove|rm) (\d+)/i).
            setDelay(3).
            setHandler(function handler(message) {
                this.channel.buildQuoteNode();
                if (!message.user.broadcaster && !message.user.moderator && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seuls les modérateurs et le broadcaster peuvent utiliser cette commande.`);
                    return false;
                }
                if (this.channel._settings.quotes.length === 0) {
                    this.channel.enqueue(`Aucune quote n'est présente dans le registre! OhMyDog`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length < 1) {
                    this.channel.enqueue(`Une erreur s'est produite lors de l'analyse de votre message, ${message.user.display}. Désolée :c`);
                    return false;
                }
                const quoteId =parseInt(regexParse[1], 10) || -1;
                if (quoteId === -1 || quoteId > this.channel._settings.quotes.length) {
                    this.channel.enqueue(`${message.user.display}, l'identifiant de quote fourni n'est pas présent dans le registre. Toutes mes excuses!`);
                    return false;
                }
                this.channel._settings.quotes.splice(quoteId-1, 1);
                this.channel.enqueue(`${message.user.display}, la quote #${quoteId} a été supprimée! Il se peut que les quotes ayant un ID supérieur a celui-ci voient leurs ID baissé de 1.`);
                this.channel.triggerUpdate();
                return true;
            });
        const quoteUpdate = new Command().
            setRegex(/^!quote (?:update|edit|set) (\d+) (.*)/i).
            setDelay(3).
            setHandler(function handler(message) {
                this.channel.buildQuoteNode();
                if (!message.user.broadcaster && !message.user.moderator && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seuls les modérateurs et le broadcaster peuvent utiliser cette commande.`);
                    return false;
                }
                if (this.channel._settings.quotes.length === 0) {
                    this.channel.enqueue(`Aucune quote n'est présente dans le registre! OhMyDog`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length < 2) {
                    this.channel.enqueue(`Une erreur s'est produite lors de l'analyse de votre message, ${message.user.display}. Désolée :c`);
                    return false;
                }
                const quoteId =parseInt(regexParse[1], 10) || -1;
                const quoteBody = regexParse[2];

                if (quoteId === -1 || quoteId > this.channel._settings.quotes.length) {
                    this.channel.enqueue(`${message.user.display}, l'identifiant de quote fourni n'est pas présent dans le registre. Toutes mes excuses!`);
                    return false;
                }
                if (quoteBody.length < 3) {
                    this.channel.enqueue(`Impossible d'ajouter une quote si elle fais moins de 3 caractères de long!`);
                }
                const quoteData = this.channel._settings.quotes[quoteId-1];
                quoteData.body = quoteBody;
                this.channel._settings.quotes.splice(quoteId-1, 1, quoteData);
                this.channel.enqueue(`${message.user.display}, la quote #${quoteId} a été mise à jour!`);
                this.channel.triggerUpdate();
                return true;
            });
            const quoteUpdateGame = new Command().
            setRegex(/^!quote update:game (\d+) (.*)/i).
            setDelay(3).
            setHandler(function handler(message) {
                this.channel.buildQuoteNode();
                if (!message.user.broadcaster && !message.user.moderator && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seuls les modérateurs et le broadcaster peuvent utiliser cette commande.`);
                    return false;
                }
                if (this.channel._settings.quotes.length === 0) {
                    this.channel.enqueue(`Aucune quote n'est présente dans le registre! OhMyDog`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length < 2) {
                    this.channel.enqueue(`Une erreur s'est produite lors de l'analyse de votre message, ${message.user.display}. Désolée :c`);
                    return false;
                }
                const quoteId =parseInt(regexParse[1], 10) || -1;
                const quoteGame = regexParse[2];

                if (quoteId === -1 || quoteId > this.channel._settings.quotes.length) {
                    this.channel.enqueue(`${message.user.display}, l'identifiant de quote fourni n'est pas présent dans le registre. Toutes mes excuses!`);
                    return false;
                }
                const quoteData = this.channel._settings.quotes[quoteId-1];
                quoteData.game = quoteGame;
                this.channel._settings.quotes.splice(quoteId-1, 1, quoteData);
                this.channel.enqueue(`${message.user.display}, la quote #${quoteId} a été mise à jour!`);
                this.channel.triggerUpdate();
                return true;
            });
        const quoteInfo = new Command().
            setRegex(/^!quote info (\d+)/i).
            setDelay(3).
            setHandler(function handler(message) {
                this.channel.buildQuoteNode();
                if (!message.user.broadcaster && !message.user.moderator && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seuls les modérateurs et le broadcaster peuvent utiliser cette commande.`);
                }
                if (this.channel._settings.quotes.length === 0) {
                    this.channel.enqueue(`Aucune Quote n'est présente dans le registre! OhMyDog`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                if (!regexParse || regexParse.length < 1) {
                    this.channel.enqueue(`Une erreur s'est produite lors de l'analyse de votre message, ${message.user.display}. Désolée :c`);
                    return false;
                }
                const quoteId =parseInt(regexParse[1], 10) || -1;
                if (quoteId === -1 || quoteId > this.channel._settings.quotes.length) {
                    this.channel.enqueue(`${message.user.display}, l'identifiant de Quote fourni n'est pas présent dans le registre. Toutes mes excuses!`);
                    return false;
                }
                const quoteData = this.channel._settings.quotes[quoteId-1];
                const quoteDate = new Date(quoteData.createdAt);
                this.channel.enqueue(`La quote #${quoteId} a été créée par ${quoteData.creator.display} le ${quoteDate.getUTCFullYear()}/${String(quoteDate.getUTCMonth()+1).padStart(2, '0')}/${String(quoteDate.getUTCDate()).padStart(2, '0')}, ${String(quoteDate.getUTCHours()).padStart(2, '0')}H${String(quoteDate.getUTCMinutes()).padStart(2, '0')} GMT${quoteData.game ? ', lors du stream sur ' + quoteData.game : ''}; Elle a été affichée ${quoteData.displays} fois depuis sa création.`);
                return true;
            });
        quote.add(quoteAdd);
        quote.add(quoteDel);
        quote.add(quoteInfo);
        quote.add(quoteUpdate);
        quote.add(quoteUpdateGame);
        return quote;
    }
    static shoutout () {
        const shoutout = new Command().
            setIgnoreDelay(true).
            setRegex(/^(?:!shoutout|!so) (\S+)/i).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                let soTarget = regexParse[1].toLowerCase();
                while (soTarget.charAt(0) === '@') {
                    soTarget = soTarget.substr(1);
                }
                // preparing informations
                const shoutoutUrl = `https://twitch.tv/${soTarget}`;
                let shoutoutDisplay = soTarget;
                const genericMessage = `N'hésitez pas à visiter la chaîne de %%user%%, à l'adresse %%link%% !`;
                const specificMessage = this.channel._settings[BuiltIns.translationKeys.so];
                let wasFound = false;

                const userInformations = ApiTwitch.getUser({ login: soTarget });
                userInformations.then((result) => {
                    if (result.found) {
                        // The user is found
                        shoutoutDisplay = result.result.display_name;
                        wasFound = true;
                    }
                }).catch((reason) => {
                    // Error while fetching the data
                    const _errMessage = 'Error while fetching informations from Twitch API: ';
                    console.error(_errMessage, reason);
                    wasFound = true;
                }).
                then(() => {
                    if (wasFound) {
                        const announcedShoutout = (specificMessage || genericMessage).replace(/%%user%%/gi, shoutoutDisplay).replace(/%%link%%/gi, shoutoutUrl);
                        this.channel.enqueue(announcedShoutout);
                    } else {
                        this.channel.enqueue(`Erm... Je n'ai pas trouvé d'informations sur cette personne :/ Vous êtes sûr qu'il s'agit de son nom de connexion? (exemple: 'belette', pas 'イタチ')`);
                    }
                });
                return true;
        });
        return shoutout;
    }
    static commandsManagement () {
        const cmd = new Command().
            setRegex(/^(?:\$|!)(?:command|cmd)/i).
            setDelay(5).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }

                this.channel.enqueue(`Sous-commandes: "!cmd add <nom> <message>" (ajoute une commande), "!cmd del <nom>" (supprime la commande !<nom>), "!cmd edit <nom> <message>" (met a jour une commande existante).`);
                return true;
            });
        const cmdAdd = new Command().
            setRegex(/^(?:\$|!)(?:command|cmd)\s(add|update|edit|set)\s(\S+)\s(.*)/i).
            setDelay(5).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                const cmdInput = `!${regexParse[2].replace(/^!+/gi, '')}`;
                const cmdOutput = regexParse[3];
                const foundMatchingCommand = this.channel._commands.find((chcmd) => {
                    return chcmd.input.toLowerCase() === cmdInput.toLowerCase();
                });
                if (foundMatchingCommand) {
                    // fix for previous commands with caps
                    foundMatchingCommand.input = cmdInput.toLowerCase();
                    // We found a matching command, we update it
                    foundMatchingCommand.output = cmdOutput;
                    foundMatchingCommand.updatedAt = new Date().toUTCString();
                    this.channel.enqueue(`La commande ${cmdInput.toLowerCase()} a été mise à jour ! VoHiYo`);
                } else {
                    // We create a new command
                    this.channel._commands.push({
                        uniqueId: uuid(),
                        createdAt: new Date().toUTCString(),
                        updatedAt: new Date().toUTCString(),
                        input: cmdInput.toLowerCase(),
                        output: cmdOutput,
                        called: 0,
                        ignoresDelay: false,
                        delay: 60,
                    });
                    this.channel.enqueue(`La commande ${cmdInput} a été créée VoHiYo`);
                }
                this.channel.triggerUpdate();
                return true;
            });
        const cmdDelete = new Command().
            setRegex(/^(?:\$|!)(?:command|cmd)\s(?:delete|del|rm)\s(\S+)/i).
            setDelay(5).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                const cmdInput = `!${regexParse[1].replace(/^!+/gi, '')}`;
                const foundMatchingCommand = this.channel._commands.findIndex((chcmd) => {
                    return chcmd.input.toLowerCase() === cmdInput.toLowerCase();
                });
                if (foundMatchingCommand < 0) {
                    this.channel.enqueue(`Aucune commande nommée ${cmdInput.toLowerCase()} n'a été trouvée :/`);
                    return false;
                }
                const deletedCommands = this.channel._commands.splice(foundMatchingCommand,1);
                if (deletedCommands.length === 0) {
                    this.channel.enqueue(`Oups.. Il semblerait que j'ai failli à mon devoir! Je suis incapable de supprimer cette commande suite à un problème interne. Désolée ! :/`);
                    return false;
                }
                this.channel.enqueue(`La commande ${cmdInput.toLowerCase()} a été supprimée VoHiYo`);
                this.channel.triggerUpdate();
                return true;
            });
        const cmdSetDelay = new Command().
            setRegex(/^(?:\$|!)(?:command|cmd)\sset:delay\s(\S+)\s(\d+)/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                const regexParse = this.command.regex.exec(message.body);
                const cmdInput = `!${regexParse[1].replace(/^!+/gi, '')}`;

                const parsedValueDelay = parseInt(regexParse[2],10);
                const delayRequested = !isNaN(parsedValueDelay) && parsedValueDelay >= 0 ? parsedValueDelay : 30;
                const foundMatchingCommand = this.channel._commands.find((chcmd) => {
                    return chcmd.input.toLowerCase() === cmdInput.toLowerCase();
                });
                if (foundMatchingCommand) {
                    // We found a matching command, we update it
                    foundMatchingCommand.delay = delayRequested;
                    foundMatchingCommand.updatedAt = new Date().toUTCString();
                    this.channel.enqueue(`La commande ${cmdInput.toLowerCase()} a désormais un délai entre deux activations de ${delayRequested} secondes ! VoHiYo`);
                } else {
                    // command not found
                    // We create a new command
                    this.channel.enqueue(`La commande ${cmdInput.toLowerCase()} n'a pas été trouvée, désolé!`);
                }
                this.channel.triggerUpdate();
                return true;
            });

        cmd.add(cmdAdd);
        cmd.add(cmdDelete);
        cmd.add(cmdSetDelay);
        return cmd;
    }
    static welcomes () {
        const wcm = new Command().
            setRegex(/^(?:\$|!)welcome/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildWelcomeNode();
                this.channel.enqueue(`Sous-commandes disponibles: "$welcome mode 0->4" (change le mode de bienvenue de Mihoka), "$welcome message <message>" (Change le message de bienvenue), "$welcome delay <delai>" (change la durée minimale entre 2 messages de bienvenue), "$welcome messageFor <user> <message>" (change le message de bienvenue  pour <user>), "$welcome deleteFor <user>" (supprime le message de bienvenue pour <user>)`);
                return true;
        });
        const wcmMode = new Command().
            setRegex(/^(?:\$|!)welcome\smode\s(\d)/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildWelcomeNode();
                const regexParse = this.command.regex.exec(message.body);
                const welcomeMode = regexParse[1];
                if (welcomeMode < 0 || welcomeMode > 2) {
                    this.channel.enqueue(`Désolée mais seul les valeurs entre 0 et 2 (inclus) sont valides. 0: Ne souhaite pas la bienvenue; 1: Souhaite la bienvenue à tous le monde au premier message, 2: souhaite la bienvenue aux VIP au premier message.`);
                    return false;
                }
                this.channel._settings.welcome.mode = welcomeMode;
                this.channel.triggerUpdate();
                const welcomeTypes = {
                    '0': `Ne pas souhaiter la bienvenue sur le chat`,
                    '1': `Souhaiter la bienvenue à tous le monde lors du premier message sur le chat`,
                    '2': `Souhaiter la bienvenue uniquement aux VIPs lors du premier message sur le chat`,
                    '3': `Souhaiter la bienvenue à tous le monde lors de l'arrivée sur le chat`,
                    '4': `Souhaiter la bienvenue uniquement aux VIPs lors de l'arrivée sur le chat`,
                }
                this.channel.enqueue(`Le mode de bienvenue est désormais le suivant: ${welcomeTypes[welcomeMode]}.`);
                return true;
        });
        const wcmMessage = new Command().
            setRegex(/^(?:\$|!)welcome\smessage\s(.*)/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildWelcomeNode();
                const regexParse = this.command.regex.exec(message.body);
                const welcomeMessage = regexParse[1];
                if (welcomeMessage.length === 0) {
                    this.channel.enqueue(`${message.user.display}, le message de bienvenue ne peut pas être vide, désolée !`);
                    return false;
                }
                this.channel._settings.welcome.message = welcomeMessage;
                this.channel.enqueue(`Le message de bienvenue est changé ! Nouveau message (%s représentant le nom de la personne arrivant): ${welcomeMessage}`);
                this.channel.triggerUpdate();
                return true;
        });
        const wcmMessageFor = new Command().
            setRegex(/^(?:\$|!)welcome\smessageFor\s(\S+)\s(.*)/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildWelcomeNode();
                const regexParse = this.command.regex.exec(message.body);
                const welcomeTarget = regexParse[1];
                const welcomeMessage = regexParse[2];
                if (welcomeMessage.length === 0) {
                    this.channel.enqueue(`${message.user.display}, le message de bienvenue ne peut pas être vide, désolée !`);
                    return false;
                }
                const userInformations = ApiTwitch.getUser({ login: welcomeTarget });
                userInformations.then((result) => {
                    if (result.found) {
                        // The user is found
                        const userId = result.result.id;
                        const userDisplay = result.result.display_name;
                        this.channel._settings.welcome.users[userId] = welcomeMessage;
                        this.channel.enqueue(`Le message de bienvenue est changé pour ${userDisplay} ! Nouveau message: ${welcomeMessage.replace('%s', userDisplay)}`);
                        this.channel.triggerUpdate();
                    } else {
                        // Unable to find user with that login
                        this.channel.enqueue(`Erm... Je n'ai pas trouvé d'informations sur cette personne :/ Vous êtes sûr qu'il s'agit de son nom de connexion?`);
                    }
                }).catch((reason) => {
                    // Error while fetching the data
                    const _errMessage = 'Error while fetching informations from Twitch API: ';
                    console.error(_errMessage, reason);
                    this.channel.enqueue(`Il semblerait qu'une erreur se soit produite durant le traitement de cette demande. Réessayez plus tard, peut-être? VoHiYo`);
                });
                return true;
        });
        const wcmDeleteFor = new Command().
            setRegex(/^(?:\$|!)welcome\sdeleteFor\s(\S+)/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildWelcomeNode();
                const regexParse = this.command.regex.exec(message.body);
                const welcomeTarget = regexParse[1].toLowerCase();
                const userInformations = ApiTwitch.getUser({ login: welcomeTarget });
                userInformations.then((result) => {
                    if (result.found) {
                        // The user is found
                        const userDisplay = result.result.display_name;
                        const userId = result.result.id;
                        if (!Reflect.get(this.channel._settings.welcome.users,userId)) {
                            this.channel.enqueue(`Il semblerait que je ne trouve aucun message de bienvenue pour cette personne, ${userDisplay} :/ Le nom est il correctement écrit ? Est-ce bien son nom de connexion et non pas d'affichage?`);
                            return false;
                        }
                        Reflect.deleteProperty(this.channel._settings.welcome.users, userId);
                        this.channel.enqueue(`Le message de bienvenue pour ${userDisplay} a été effacé !`);
                        this.channel.triggerUpdate();
                        return true;
                    }
                    // Unable to find user with that login
                    this.channel.enqueue(`Erm... ${message.user.display}?!? Je n'ai pas trouvé d'informations sur ${welcomeTarget} :/ Vous êtes sûr qu'il s'agit de son nom de connexion?`);
                    return false;
                }).catch((reason) => {
                    // Error while fetching the data
                    const _errMessage = 'Error while fetching informations from Twitch API: ';
                    console.error(_errMessage, reason);
                    this.channel.enqueue(`Il semblerait qu'une erreur se soit produite durant le traitement de cette demande. Réessayez plus tard, peut-être? VoHiYo`);
                    return false;
                });
                return true;
            });
        const wcmDelay = new Command().
            setRegex(/^(?:\$|!)welcome\sdelay\s(\d+)/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                    return false;
                }
                this.channel.buildWelcomeNode();
                const regexParse = this.command.regex.exec(message.body);
                const welcomeDelay = regexParse[1];
                if (welcomeDelay < 60) {
                    this.channel.enqueue(`Désolée mais seul les délais supérieurs ou égaux à 60 (minutes) sont autorisés !`);
                    return false;
                }
                this.channel._settings.welcome.delay = welcomeDelay;
                this.channel.enqueue(`Le délai entre deux messages de bienvenue pour une personne est désormais de ${welcomeDelay} minutes VoHiYo`);
                this.channel.triggerUpdate();
                return true;
        });
        wcm.addAll([wcmMode,wcmMessage,wcmMessageFor,wcmDelay,wcmDeleteFor]);
        return wcm;
    }
    static fc () {
        const fcc = new Command().setRegex(/^!fc/i).
            setIgnoreDelay(true).
            setHandler(function handler(message) {
                if (message.user.id === this.channel._id) {
                    this.channel.enqueue(`${message.user.display}... Je ne sais comment le dire mais... Vous ne pouvez pas vous suivre vous-même... désolée :( `);
                    return true;
                }
                const dateTimeFormat = new Intl.DateTimeFormat('fr', {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                });
                const respFC = ApiTwitch.isFollowing(message.user.id, this.channel._id);
                respFC.then((result) => {
                    if (result.follows) {
                        const sinceDate = new Date(result.since);
                        const currDate = new Date().valueOf();
                        const minutesDiff = Math.floor((currDate - sinceDate.valueOf()) / 60000);
                        const diff = {
                            y: Math.floor(minutesDiff / (60 * 24 * 30 * 12)),
                            m: Math.floor(minutesDiff / (60 * 24 * 30)%12),
                            d: Math.floor(minutesDiff / (60 * 24)%30),
                            h: Math.floor(minutesDiff / 60 % 24),
                            mm: Math.floor(minutesDiff % 60),
                        };

                        this.channel.enqueue(`${message.user.display}, vous suivez la chaîne depuis le ${dateTimeFormat.format(sinceDate)} (${
                            diff.y > 0 ? `${diff.y} année(s), ` : ''
                        }${
                            diff.m > 0 ? `${diff.m} mois, ` : ''
                        }${
                            diff.d > 0 ? `${diff.d} jour(s), ` : ''
                        }${
                            diff.h > 0 ? `${diff.h} heure(s), ` : ''
                        }${
                            diff.mm > 0 ? `${diff.mm} minute(s)` : ''
                        }.`);
                    } else {
                        this.channel.enqueue(`${message.user.display}, vous ne suivez pas la chaîne.`);
                    }
                    return true;
                }).catch((reason) => {
                    this.channel.enqueue(`Désolée mais j'ai eu un petit problème technique et ne suis pas en mesure de vous donner la date de follow pour le moment VoHiYo`);
                    const _errMessage = 'ERROR FETCHING FOLLOW: ';
                    console.error(_errMessage, reason);
                });
                return false;
        });
        return fcc;
    }
    static mihoka () {
        const mhk = new Command().setRegex(/^!mihoka/i).
            setHandler(function handler() {
                this.channel.enqueue(`Je suis Mihoka, une entité servant les streamers dans leurs tâches journalières ! Je suis actuellement en version ${AppInformations.VERSION} et est ouverte à toute suggestion! Je vous invite à  contacter mon créateur Lumikkode pour cela. Mes commandes sont disponibles à https://docs.mihoka.me/`);
                return true;
        });
        return mhk;
    }
    static roll () {
        const roll = new Command().
            setRegex(/^!(?:roll|dice) (-?\d+)/i).
            setHandler(function handler(message) {
                const parsedArgs = this.command.regex.exec(message.body);
                const numMax = parseInt(parsedArgs[1],10);
                if (isNaN(numMax)) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais ${parsedArgs[1]} n'est pas un nombre!`);
                    return false;
                }
                if (numMax < 1) {
                    this.channel.enqueue(`Désolée ${message.user.display} mais un lancé de dé requiert un nombre positif !`);
                    return false;
                }
                if (numMax === 1) {
                    this.channel.enqueue(`... ${message.user.display} ? Tu ... Tu as vraiment besoin de connaître un nombre aléatoire entre 1 et 1 ? Devine... Aller, tu peux le faire... Non ? Bon, ça fais 1!`);
                    return false;
                }
                const randomNumber = Math.floor(Math.random() * numMax) + 1;
                this.channel.enqueue(`*lance un dé* ...  ${message.user.display} obtient ${randomNumber} sur un lancé de dé 1 à ${numMax}`);
                return true;
        });
        return roll;
    }
    static customize() {
        const customize = new Command().
        setIgnoreDelay(true).
        setRegex(/^!customize (so|raid|gift|sub|sub1|sub2|sub3|subprime)\s(.*)/i).
        setHandler(function handler(message) {
            if (!message.user.moderator && !message.user.broadcaster && !message.user.creator) {
                this.channel.enqueue(`Désolée ${message.user.display} mais seul les modérateurs et le streamer ont accès à cette commande.`);
                return false;
            }
            const regexParse = this.command.regex.exec(message.body);
            const cmdType = String(regexParse.length && regexParse[1]).toLowerCase();
            const content = String(regexParse.length > 2 && regexParse[2]).trim();
            const friendlyName = {
                so: 'Shoutouts/Promotions',
                raid: 'Raids',
                gift: 'Cadeau',
                sub: 'Abonnements (défaut)',
                sub1: 'Abonnements (Tier 1)',
                sub2: 'Abonnements (Tier 2)',
                sub3: 'Abonnements (Tier 3)',
                subprime: 'Abonnements (Twitch Prime)',
            };
            const translationKey = BuiltIns.translationKeys[cmdType];
            this.channel._settings[translationKey] = content;
            this.channel.enqueue(`Message pour "${friendlyName[cmdType]}" mis à jour!`);
            this.channel.triggerUpdate();
            return true;
        });
        return customize;
    }
    static cmdCustomElkinooJeux() {
        return new Command().
            setRegex(/^!jeux/i).
            setDelay(60).
            setHandler(function handler() {
                // if not elkinoo's channel, stop all
                if (this.channel._name !== 'elkinoo') {
                    return false;
                }
                // apply logic
                const gsheetsCall = superagent.get('https://docs.google.com/spreadsheets/d/1Zmj6zBGx905JIuOH_TlLuLzaebrNQyCyIxcvsIVyxL4/export?format=csv&id=1Zmj6zBGx905JIuOH_TlLuLzaebrNQyCyIxcvsIVyxL4&gid=0').send();

                gsheetsCall.then((res) => {
                    const validLines = res.text.split('\r\n').
                        map((l) => l.split(',')).
                        map((l) => {
                            for (const col in l) {
                                if (l[col].startsWith('MàJ: ')) {
                                    return l[col].
                                        split(' ').
                                        filter((z) => z).
                                        join(' ');
                                }
                                if (l[col].startsWith('Jeux ') && l[col].endsWith(':')) {
                                    return [l[col], l[Number(col)+1]].join(' ');
                                }
                            }
                            return null
                        }).
                        filter((x) => x);
                    this.channel.enqueue(`Liste des jeux sur le Canap': ${validLines.join(' | ')}`);
                })
                return true;
            });
    }
    constructor() {
        this._commands = BuiltIns.commands();
    }
    invoke(message, channelRelation) {
        return this._commands.find((cmd) => {
            if (cmd.triggeredBy(message)) {
                cmd.execute(message,{
                    command: cmd,
                    channel: channelRelation,
                });
                return true;
            }
            return false;
        });
    }
}
BuiltIns.translationKeys = {
    so: 'translations.custom.shoutout',
    raid: 'translations.custom.raid',
    sub: 'translations.custom.subscription',
    gift: 'translations.custom.subscription-gift',
    sub1: 'translations.custom.subscription-tier1',
    sub2: 'translations.custom.subscription-tier2',
    sub3: 'translations.custom.subscription-tier3',
    subprime: 'translations.custom.subscription-prime',
};
module.exports = BuiltIns;