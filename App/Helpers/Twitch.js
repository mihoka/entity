module.exports = class TwitchHelper {
    static get serverAddress() {
        return 'irc.chat.twitch.tv';
    }
    static get serverPort() {
        return 6667;
    }
    static hostnameRegex(line) {
        return (/^Your host is (\S+)/gi).exec(line);
    }
    static capabilityRegex(line) {
        return (/^:(\S+) CAP \* (ACK|NAK) :(\S+)/i).exec(line);
    }
    static messageRegex(line) {
        return (/^(@(\S+)\s)?(:(\S+)\s(\S+)\s(\S+)(\s:(.*))?)/gi).exec(line);
    }
    static getHostname(response) {
        const res = this.hostnameRegex().exec(response);
        return res === null ? null : res[1];
    }
    static capabilities() {
        return [
            'twitch.tv/membership',
            'twitch.tv/commands',
            'twitch.tv/tags'
        ];
    }
    static alreadyBannedRegex (line) {
        return (/^(\S+) is already banned in this room./gi).exec(line);
    }
    static currentlyHostingRegex(line) {
        return (/^This channel is hosting (\S+)./gi).exec(line);
    }
    static notBannedRegex(line) {
        return (/^(\S+) is not banned from this room./gi).exec(line);
    }
    static bannedRegex(line) {
        return (/^(\S+) is banned from this room./gi).exec(line);
    }
    static hostingStartedRegex(line) {
        return (/^Now hosting (\S+)./gi).exec(line);
    }
    static remainingHostsRegex(line) {
        return (/^There are (\S+) host commands remaining this half hour./gi).exec(line);
    }
    static slowModeEnabledRegex(line) {
        return (/^This room is now in slow mode. You may send messages every (\d) seconds./i).exec(line);
    }
    static timedOutRegex(line) {
        return (/^(\S+) has been timed out for (\d) seconds./gi).exec(line);
    }
    static unbannedRegex(line) {
        return (/^(\S+) is no longer banned from this chat room./gi).exec(line);
    }
    static unrecognizedCommandRegex(line) {
        return (/^Unrecognized command: (.*)/gi).exec(line);
    }
}