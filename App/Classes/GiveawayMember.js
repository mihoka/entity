const uuid = require('uuid/v4');
module.exports = class GiveawayMember {
    constructor(parentId, userData = null) {
        this._parentId = parentId;
        this._createdAt = new Date().toUTCString();
        this._updatedAt = new Date().toUTCString();
        this._uniqueId = uuid();
        this._display = null;
        this._id = null;
        this._login = null;
        this._rawUser = userData;
        if (userData) {
            this._display = userData.display;
            this._id = userData.id;
            this._login = userData.login;
            if (userData.uniqueId) {
                this._uniqueId = userData.uniqueId;
            }
        } else {
            return false;
        }
    }

    get login() {
        return this._login;
    }
    get display() {
        return this._display;
    }
    get id() {
        return this._id;
    }

    get createdAt() {
        return this._createdAt;
    }
    get updatedAt() {
        return this._updatedAt;
    }
    get uniqueId() {
        return this._uniqueId;
    }

    equals(obj) {
        // Checks if given obj matches the current object
        if (Reflect.getPrototypeOf(obj) !== Reflect.getPrototypeOf(this)) {
            return false;
        }
        if (!obj._uniqueId) {
            return false;
        }
        if (!this._uniqueId) {
            return false;
        }
        if (obj._uniqueId === this._uniqueId) {
            return true;
        }
        if (obj._id === this._id) {
            return true;
        }
        return false;
    }
    toJSON() {
        return {
            giveawayId: this._parentId,
            createdAt: this._createdAt,
            updatedAt: this._updatedAt,
            uniqueId: this._uniqueId,
            display: this._display,
            id: this._id,
            login: this._login,
        };
    }
}