const GiveawayMember = require('App/Classes/GiveawayMember');
const uuid = require('uuid/v4');

module.exports = class Giveaway {
    static get STATUS_RUNNING() {
        return 'running';
    }
    static get STATUS_CLOSED() {
        return 'closed';
    }
    static get STATUS_ENDED() {
        return 'ended';
    }

    constructor() {
        this._createdAt = new Date().toUTCString();
        this._updatedAt = new Date().toUTCString();
        this._channelId = null;
        this._uniqueId = uuid();
        this._members = [];
        this._winners = [];
        this._status = Giveaway.STATUS_RUNNING;
    }

    static fromObject(obj) {
        if (!obj) {
            return null;
        }
        const ga = new Giveaway();
        ga._createdAt = obj._createdAt;
        ga._updatedAt = obj._updatedAt;
        ga._channelId = obj._channelId;
        ga._uniqueId = obj._uniqueId;
        ga._members = [];
        ga._winners = [];
        ga._status = obj._status;

        obj._members.forEach((m) => {
            ga._members.push(new GiveawayMember(m.giveawayId, m));
        });
        obj._winners.forEach((m) => {
            ga._winners.push(new GiveawayMember(m.giveawayId, m));
        });
        return ga;
    }

    get uniqueId() {
        return this._uniqueId;
    }
    get createdAt() {
        return this._createdAt;
    }
    get updatedAt() {
        return this._updatedAt;
    }
    get channelId() {
        return this._channelId;
    }
    get members() {
        return this._members;
    }
    get status() {
        return this._status;
    }
    get statusName() {
        const _statustr = {
            running: 'en cours',
            closed: 'inscriptions fermées',
            ended: 'terminé',
        }
        return _statustr[this._status];
    }
    set status(val) {
        return this._status;
    }
    get winners() {
        return this._winners;
    }
    set channelId(uniqueId) {
        this._channelId = uniqueId;
        this.update();
    }
    load(payload) {
        // Loading the content of the Giveaway with a payload
        if (!payload) {
            return false;
        }
        if (!payload.createdAt) {
            return false;
        }
        if (!payload.channelId) {
            return false;
        }
        if (!payload.uniqueId) {
            return false;
        }
        if (!payload.members) {
            return false;
        }
        if (!payload.winners) {
            return false;
        }
        this._updatedAt = payload.updatedAt?payload.updatedAt:null;
        this._createdAt = payload.createdAt;
        this._channelId = payload.channelId;
        this._uniqueId = payload.uniqueId;
        this._members = payload.members;
        this._winners = payload.winners;
        return true;
    }
    set members(memberVal) {
        this._members = memberVal;
        this.update();
    }
    set winners(winnerVal) {
        this._winners = winnerVal;
        this.update();
    }
    get nonWinners() {
        if (this._members.length === 0) {
            // members is empty
            return [];
        }
        if (this._winners.length === 0) {
            // No winners yet, we return directly the members array
            return [...this._members];
        }
        if (this._members.length === this._winners.length) {
            // Arrays are same size, we return directly an empty array
            return [];
        }
        const that = this;
        return this._members.filter((element) => {
            return !that.hasWinner(element.toJSON());
        });
    }
    close() {
        this._status = Giveaway.STATUS_CLOSED;
    }
    open() {
        this._status = Giveaway.STATUS_RUNNING;
    }
    end() {
        this._status = Giveaway.STATUS_ENDED;
    }
    isRunning() {
        return this._status === Giveaway.STATUS_RUNNING;
    }
    isClosed() {
        return this._status === Giveaway.STATUS_CLOSED;
    }
    isEnded() {
        return this._status === Giveaway.STATUS_ENDED;
    }
    update() {
        this._updatedAt = new Date().toUTCString();
    }
    draw(pCount) {
        if (this._status !== Giveaway.STATUS_CLOSED) {
            this.close();
        }
        let count = pCount;
        const vNonWinners = this.nonWinners;
        // We draw a winner
        if (vNonWinners.length > 0) {
            // The array has still members who did not win
            if (!count || !isNaN(count) || count < 1) {
                count = 1;
            }
            if (vNonWinners.length < count) {
                // Draw count is bigger than available members
                this.end();
                return {
                    total: vNonWinners.length,
                    flags: ['ok','alldrawn'],
                    drawn: vNonWinners,
                };
            }
            // Drawing from the list
            const drawnMembers = [];
            for (let i = 0; i < count; i+=1) {
                const indexMember = this.randomizeIndex(vNonWinners.length);
                let winner = vNonWinners.splice(indexMember, 1);
                if (winner.length > 0) {
                    winner = winner[0];
                    drawnMembers.push(winner);
                    this._winners.push(winner)
                }
            }
            return {
                total: count,
                flags: ['ok'],
                drawn: drawnMembers,
            };
        }
        // Empty array
        return {
            total: 0,
            flags: ['alldrawn'],
            drawn: [],
        };
    }
    add(user) {
        // We add a member from a Message.User object
        if (!this.hasMember(user)) {
            const userMember = new GiveawayMember(this._uniqueId, user);
            this._members.push(userMember);
            this.update();
        }
    }
    randomizeIndex(lastIndex) {
        const _randomize = {};
        const maxFound = {
            index: null,
            value: 0,
        }
        for (let j = 0; j < 101; j+=1) {
            const indexMember = Math.floor(Math.random() * lastIndex);
            if (Reflect.has(_randomize, indexMember)) {
                _randomize[indexMember] += 1;
            } else {
                _randomize[indexMember] = 1;
            }
            if (maxFound.value < _randomize[indexMember]) {
                maxFound.value = _randomize[indexMember];
                maxFound.index = indexMember;
            }
        }
        // find highest index occurence
        return maxFound.index;
    }
    find(user) {
        this.findMember(user);
    }
    findMember(userData) {
        // Checks if the current giveaway has the member
        const memberObj = new GiveawayMember(this._uniqueId, userData);
        if (!memberObj) {
            return null;
        }
        return this._members.find((element) => {
            return element.equals(memberObj);
        });
    }
    findWinner(userData) {
        // Checks if the current giveaway has the member
        const memberObj = new GiveawayMember(this._uniqueId, userData);
        if (!memberObj) {
            return null;
        }
        return this._winners.find((element) => {
            return element.equals(memberObj);
        });
    }
    hasMember(user) {
        return typeof this.findMember(user) !== 'undefined';
    }
    hasWinner(user) {
        return typeof this.findWinner(user) !== 'undefined';
    }
    hasMembers() {
        return this._members.length>0;
    }
    hasWinners() {
        return this._winners.length>0;
    }
}