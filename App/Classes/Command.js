const uuid = require('uuid/v4');
module.exports = class Command {
    constructor() {
        this._commands = [];
        this._uniqueId = uuid();
        this._regex = /^!\?/i;
        this._delay = 3;
        this._ignoresDelay = false;
        this._calledAt = 0;
        this._handler = function _handler() { return false; };
    }
    execute(message, context) {
        if (!this.triggeredBy(message)) {
            return false;
        }
        const command = this._commands.find((cmd) => {
            return cmd.triggeredBy(message);
        });
        if (command) {
            context.command = command;
            return command.execute(message, context);
        }
        this._calledAt = Date.now();
        context.command = this;
        return Reflect.apply(this._handler, context, [message]);
    }
    triggeredBy(message) {
        return this._regex.test(message.body);
    }
    add(commandObj) {
        return this._commands.push(commandObj);
    }
    addAll(arr) {
        return this._commands.push(...arr);
    }
    remove(uniqueId) {
        const index = this._commands.findIndex((elm) => {
            return elm._uniqueId === uniqueId;
        });
        if (index > -1) {
            this._commands.splice(index,1);
        }
    }
    setNoDelay() {
        this._delay = 0;
        this._ignoresDelay = true;
    }
    set delay (delay) {
        if (delay < 0) {
            return this._delay;
        }
        this._delay = delay;
        return delay;
    }
    get delay () {
        return this._delay;
    }
    get uniqueId () {
        return this._uniqueId;
    }
    set uniqueId (id) {
        return this._uniqueId;
    }
    set ignoresDelay (value) {
        if (typeof value === 'boolean') {
            this._ignoresDelay = value;
        }
        return this._ignoresDelay;
    }
    get ignoresDelay () {
        return this._ignoresDelay;
    }
    get callTime () {
        return this._calledAt;
    }
    get regex () {
        return this._regex;
    }
    set regex (rx) {
        if (rx instanceof RegExp) {
            this._regex = rx;
        }
        return this._regex;
    }
    get handler () {
        return this._handler;
    }
    set handler (fnc) {
        if (typeof fnc === 'function') {
            this._handler = fnc;
        }
        return this._handler;
    }
    // Chained commands
    setHandler(fnc) {
        this.handler = fnc;
        return this;
    }
    setRegex(reg) {
        this.regex = reg;
        return this;
    }
    setIgnoreDelay(bool) {
        this.ignoresDelay = bool;
        return this;
    }
    setUniqueId(uniqid) {
        this.uniqueId = uniqid;
        return this;
    }
    setDelay(delay) {
        this.delay = delay;
        return this;
    }
}