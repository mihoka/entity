const TwitchIrcClass = require('./Irc');
const TwitchApiClass = require('./Api');
module.exports = {
  'Irc': TwitchIrcClass,
  'Api': TwitchApiClass,
}