const ApiCall = require('App/Controllers/Common/ApiCall');
const TwitchConfig = require('App/Config/Api').Twitch;
const superagent = require('superagent');

class Api {
  static async getBearerId() {
    return await superagent.post(`https://id.twitch.tv/oauth2/token?client_id=${TwitchConfig.clientId}&client_secret=${TwitchConfig.secretId}&grant_type=client_credentials`);
  }
  static isFollowing(userId, channelId) {
    return new Promise((resolve, reject) => {
      if (userId === channelId) {
        // TODO return creation date of the channel, once creation_date is on Helix APIs
      }
      const call = new ApiCall(TwitchConfig.hostname, '/helix/users/follows?to_id='+channelId+'&from_id='+userId, 'GET', 443, true, {
        'Client-Id': TwitchConfig.clientId,
        'Authorization': `Bearer ${TwitchConfig.bearerId}`
      }).execute();
      call.then((result) => {
        if (!result || !result.body || !result.body.data) {
          reject('invalid response');
        }
        // The request succeeded
        if (result.body.data.total === 1) {
          // User follows
          resolve({ follows: true, since: result.body.data.data[0].followed_at });
        } else {
          // User does not follow
          resolve({ follows: false, since: null });
        }
      }).catch((reason) => {
        // An error happened
        reject(reason);
      });
    });
  }
  static getUser(userParams) {
    return new Promise((resolve, reject) => {
      let requestPath = '/helix/users?';
      if (userParams.login) {
        // Per login search
        requestPath += `login=${userParams.login}`;
      } else if (userParams.id) {
        // Per id search
        requestPath += `id=${userParams.id}`;
      }
      const requestCall = new ApiCall(TwitchConfig.hostname, requestPath, 'GET', 443, true, {
        'Client-Id': TwitchConfig.clientId,
        'Authorization': `Bearer ${TwitchConfig.bearerId}`
      }).execute();
      requestCall.then((result) => {
        if (!result || !result.body || !result.body.data) {
          reject('invalid response');
        }
        if (result.body.data.data.length > 0) {
          resolve({ found: true, result: result.body.data.data[0] });
        }
        resolve({ found: false, result: result.body.data });
      }).catch((reason) => {
        reject(reason);
      });
    });
  }
  static getCurrentStream(userParams) {
    return new Promise((resolve, reject) => {
      let requestPath = '/helix/streams?';
      if (userParams.login) {
        // Per login search
        requestPath += `user_login=${userParams.login}`;
      } else if (userParams.id) {
        // Per id search
        requestPath += `user_id=${userParams.id}`;
      }
      const requestCall = new ApiCall(TwitchConfig.hostname, requestPath, 'GET', 443, true, {
        'Client-Id': TwitchConfig.clientId,
        'Authorization': `Bearer ${TwitchConfig.bearerId}`
      }).execute();
      requestCall.then((result) => {
        if (!result || !result.body || !result.body.data) {
          reject('invalid response');
        }
        if (result.body.data.data.length > 0) {
          resolve({ found: true, result: result.body.data.data[0] });
        }
        resolve({ found: false, result: result.body.data });
      }).catch((reason) => {
        reject(reason);
      });
    });
  }
  static getGame(gameId) {
    return new Promise((resolve, reject) => {
      const requestPath = '/helix/games?id=' + gameId;
      const requestCall = new ApiCall(TwitchConfig.hostname, requestPath, 'GET', 443, true, {
        'Client-Id': TwitchConfig.clientId,
        'Authorization': `Bearer ${TwitchConfig.bearerId}`
      }).execute();
      requestCall.then((result) => {
        if (!result || !result.body || !result.body.data) {
          reject('invalid response');
        }
        if (result.body.data.data.length > 0) {
          resolve({ found: true, result: result.body.data.data[0] });
        }
        resolve({ found: false, result: result.body.data });
      }).catch((reason) => {
        reject(reason);
      });
    });
  }
}

setInterval(() => {
  console.log(process.pid, "** Fetching new BearerId");
  Api.getBearerId().then((res) => {
    TwitchConfig.bearerId = res.body.access_token;
    console.log(process.pid, "** Finished fetching new token for pid");
  }).catch(console.error);
}, 3600*1000);
module.exports = Api;