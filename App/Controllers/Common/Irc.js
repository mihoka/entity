/* eslint-disable max-lines */
const net = require('net');
const chalk = require('chalk');
const env = require('App/Helpers/Env');
const IrcHelper = require('App/Helpers/Irc');
const TwitchHelper = require('App/Helpers/Twitch');

module.exports = class Irc {
    constructor() {
        this.channels = {};
        this.authenticated = false;
        this.connected = false;
        this.reconnect = true;

        this.requiredCapabilities = TwitchHelper.capabilities();
        this.enqueuedCapabilities = [...this.requiredCapabilities];
        this.capabilities = { successful: [], failed: [], required: [], enqueued: [] };

        this.bufferedMessage = null;
        this.socket = null;
        this.activity = {
            lastAnswer: 0,
            ping: 0,
            ticker: 0,
            count: 0,
        }
    }

    /**
     * Creates the socket for a new connection to be established
     * @returns {void} nothing
     * @version 1.0
     */
    createSocket() {
        if (this.socket && !this.socket.destroyed) {
            return;
        }
        this.socket = new net.Socket();
        this.socket.setEncoding('utf8');
        this.socket.setKeepAlive(true, 3000);
        this.socket.setNoDelay();

        this.socket.on('error', (err) => {
            this.onError(err);
        });
        this.socket.on('close', (hadError) => {
            this.onClose(hadError);
        });
        this.socket.on('timeout', () => {
            this.onTimeout();
        });
        this.socket.on('end', () => {
            this.onEnd();
        })
        this.socket.on('data', (data) => {
            this.onData(data);
        });
        this.socket.on('connect', () => {
            this.onConnect();
        });
    }

    /**
     * Joins a specified channel, either by name (string) or object (Channel class instance)
     * @param {Channel} channel the channel to join
     * @returns {void} nothing
     * @version 1.0
     */
    join(channel) {
        this.raw('JOIN ' + this.getChannelTag(channel));
        return true;
    }

    /**
     * leaves a specified channel, either by name (string) or object (Channel class instance)
     * @param {Channel} channel the channel to leave
     * @returns {void} nothing
     * @version 1.0
     */
    part(channel) {
        this.raw('PART ' + this.getChannelTag(channel));
    }

    /**
     * sends a message to a specific channel, without verifying its integrity.
     * @param {Channel} channel the channel where the message will be sent
     * @param {String} message the message to send to the channel
     * @returns {void} nothing
     * @version 1.0
     */
    sendMessage(channel, message) {
        this.raw('PRIVMSG ' + this.getChannelTag(channel) + ' :' + message);
    }

    /**
     * Sends a /me command to the channel, with 'message' as an action.
     * @param {Channel} channel the channel where the action will be sent
     * @param {String} action the action to display
     * @returns {void} nothing
     * @version 1.0
     */
    action(channel, action) {
        this.sendMessage(this.getChannelTag(channel), '.me ' + action);
    }

    /**
     * Sends a private message to a user specifically, and not a whole channel
     * @param {User} user the user to contact privately
     * @param {String} message the message to send to the user
     * @returns {void} nothing
     * @version 1.0
     */
    whisper(user, message) {
        this.raw('PRIVMSG #jtv :/w ' + user + ' ' + message);
    }

    /**
     * Sends raw data of your choice to the server
     * @param {String} data the raw data to send, in UTF-8
     * @returns {void} nothing
     * @version 1.0
     */
    raw(data) {
        if (this.socket && !this.socket.destroyed && this.connected) {
            this.socket.write(data + '\r\n', 'utf8');
        }
    }

    /**
     * handles the raw data received by the socket form the server, to dispatch it to 'on()' methods of the class.
     * @param {String} raw the raw data received
     * @returns {void} nothing
     * @version 1.2
     */
    handleRawData(raw) {
        this.activity.lastAnswer = Date.now();
        const messageParsed = IrcHelper.parse(raw);
        if (IrcHelper.isPing(raw)) {
            this.onPing(raw);
        } else if (IrcHelper.isPong(raw)) {
            this.onPong(raw);
        } else if (messageParsed === null) {
            // error
            this.onUnknown(messageParsed);
        } else {
            this.parseMessage(messageParsed);
        }
    }

    parseMessage(result) {
        if (result === null) {
            return null;
        }
        const tagsParsed = IrcHelper.parseTags(result[1]);
        const badgesParsed = IrcHelper.parseBadges(tagsParsed.badges);
        const message = {
            raw: result,
            tags: tagsParsed,
            user: {
                badges: badgesParsed,
                login: tagsParsed.count === 0 ? '' : tagsParsed.login,
                display: tagsParsed.count === 0 ? '' : tagsParsed['display-name'],
                id: tagsParsed.count === 0 ? '' : tagsParsed['user-id'],
                moderator: tagsParsed.count > 0 && tagsParsed.mod === '1',
                broadcaster: Reflect.apply(Object.prototype.hasOwnProperty, badgesParsed, ['broadcaster']),
                creator: tagsParsed.count === 0 ? false : tagsParsed['user-id'] === '51175553',
                admin: false,
                subscriber: tagsParsed.count > 0 && tagsParsed.subscriber === '1',
            },
            type: result[3],
            channel: result[5],
            body: result[7],
        }
        // Redirecting the formatted message to the handler manager
        this.messageToHandler(message);
        return true;
    }

    /**
     * handles a message sent from the server to the socket, which has a IRCv3 tag attached to it.
     * @param {String} message the structured message to handle
     * @returns {void} nothing
     * @version 1.2
     */
    messageToHandler(message) {
        switch (message.type) {
            case 'PRIVMSG': this.onMessage(message); break;
            case 'CLEARCHAT': this.onClear(message); break;
            case 'ROOMSTATE': this.onRoomState(message); break;
            case 'NOTICE': this.onNotice(message); break;
            case 'USERSTATE': this.onUserState(message); break;
            case 'RECONNECT': this.onReconnect(message); break;
            case 'USERNOTICE': this.onUserNotice(message); break;
            case 'JOIN': this.onJoin(message); break;
            case 'PART': this.onPart(message); break;
            case 'HOSTTARGET': this.onHostTarget(message); break;
            case 'CAP * ACK': case 'CAP * NAK': this.onCapability(message); break;
            case 'MODE': this.onMode(message); break;
            default: this.handleMotd(message); break;
        }
    }

    /**
     * checks wether this is a MOTD message, and redirects to the correct handler
     * @param {String} message the message to handle
     * @returns {void} nothing
     * @version 1.0
     */
    handleMotd(message) {
        const isNumeric = !isNaN(message.type);
        if (isNumeric) {
            this.onWelcome(message);
        } else {
            this.onUnknown(message);
        }
    }

    onJoin(message) {
        const channel = this.channels[this.getChannelName(message.channel)];
        channel.poll();
    }

    onReconnect(message) {
        console.log(chalk.yellow(new Date().toUTCString()), Object.keys(this.channels).join(';'), ' Server requested us to reconnect.', JSON.stringify(message));
        this.reconnect = true;
    }

    onPart(message) {
        return message;
    }

    onMode(message) {
        return message;
    }

    onCapability(message) {
        const parsed = TwitchHelper.capabilityRegex(message.raw[0]);
        if (this.addCapability(parsed) && this.hasEndedCapabilities()) {
            if (this.hasChannels()) {
                this.joinChannels();
            } else {
                this.disconnect();
            }
        }
    }

    addCapability(parsed) {
        if (Array.isArray(parsed)) {
            const acquired = parsed[2] === 'ACK';
            this.capabilities[acquired ? 'successful' : 'failed'].push(parsed[3]);
            return true;
        }
        return false;
    }

    hasChannels() {
        return Object.keys(this.channels).length > 0;
    }

    hasEndedCapabilities() {
        return this.requiredCapabilities.length === this.capabilities.successful.length + this.capabilities.failed.length && this.enqueuedCapabilities.length === 0;
    }

    joinChannels() {
        Object.keys(this.channels).forEach((e) => {
            this.join(`#${e}`);
        });
    }

    /**
     * Handles a ping sent from the server to our IRC socket
     * @param {String} raw the ping request, in a raw format
     * @returns {void} nothing
     * @version 1.0
     */
    onPing(raw) {
        this.activity.ping = Date.now();
        this.pong(raw);
    }

    /**
     * Handles a PONG answer from the server to our IRC socket, after we requested a PING
     * @param {String} raw the raw response from the server
     * @returns {void} nothing
     * @version 1.0
     */
    onPong() {
        this.activity.ping = Date.now();
    }

    onUnknown(message) {
        return message;
    }

    onMessage(message) {
        const channel = this.getChannel(message.channel);
        if (channel) {
            channel.handleMessage(message);
        }
    }

    onClear(message) {
        return message;
    }

    onRoomState(message) {
        return message;
    }

    onUserState(message) {
        const ch = this.getChannel(message.channel);
        if (ch) {
            ch.onUserState(message);
        }
        return message;
    }

    onSub(message) {
        const channel = this.getChannel(message.channel);
        if (channel) {
            channel.handleSub(message);
        }
    }
    onRaid(message) {
        const channel = this.getChannel(message.channel);
        if (channel) {
            channel.handleRaid(message);
        }
    }

    onUserNotice(message) {
        switch (message.tags['msg-id']) {
            case 'sub':
            case 'resub':
            case 'subgift':
            this.onSub(message);
            break;

            case 'raid':
            this.onRaid(message);
            break;

            default:
        }
        return message;
    }

    /**
     * Handles the MOTD messages from a server, on connection of the socket and successful authentication.
     * @param {String} message the message sent from the server as a MOTD row
     * @returns {void} nothing
     * @version 1.0
     */
    onWelcome(message) {
        switch (message.type) {
            case 1: case '001':
                // Welcome, GLHF
                break;
            case 2: case '002':
                // Server name
                this.setHostname(message);
                break;
            case 3: case '003':
            case 4: case '004':
            case 353: case '353':
            case 366: case '366':
            case 372: case '372':
            case 375: case '375':
                break;
            case 376: case '376':
                // MOTD End
                this.authenticated = true;
                this.requestCapacities();
                break;
            default:
                this.onUnknown(message);
        }
    }

    /**
     * Handles the NOTICE messages from the IRC server to which we are connected via the socket
     * @param {Object} message the message sent with the Notice
     * @returns {void} nothing
     * @version 1.0
     */
    onNotice(message) {
        return message;
    }

    /**
     * Handles the notice from a remote user that hosted the current channel for a specific amount of spectators
     * @param {Message} message the message node
     * @returns {void} nothing
     * @version 1.0
     */
    onHostTarget(message) {
        return message;
    }

    /**
     * Handles the 'timeout' event (when the socket does not receive anything since a specific delay) of the IRC Socket. Allows to do hooks once the event is triggered
     * @returns {void} nothing
     * @version 1.0
     */
    onTimeout() {
        this.connected = false;
        this.authenticated = false;
        console.error(chalk.magenta(new Date().toUTCString()), Object.keys(this.channels).join(';'), 'timeout of connection.');
    }

    /**
     * Handles the 'end' event (when the socket stops listening to any new data from the server) of the IRC Socket. Allows to do hooks once the event is triggered
     * @returns {void} nothing
     * @version 1.0
     */
    onEnd() {
        console.error(chalk.magenta(new Date().toUTCString()), Object.keys(this.channels).join(';'), 'The server closed the connection.');
        this.connected = false;
        this.authenticated = false;
    }

    /**
     * Handles the 'connect' event (when the socket successfully connects to the remote) of the IRC Socket. Allows to do hooks once the event is triggered
     * @returns {void} nothing
     * @version 1.0
     */
    onConnect() {
        this.connected = true;
        this.activity.ping = Date.now();
        this.verifyPingability();
        setTimeout(() => { this.authenticate(); }, 1000);
    }

    /**
     * Handles the 'data' event (when the socket receives input from the server) of the IRC Socket. Allows to do hooks once the event is triggered
     * @param {String} data the data sent from the server to us
     * @returns {void} nothing
     * @version 1.0
     */
    onData(data) {
        let _data = data;
        if (this.isBuffered(_data)) {
            this.buffer(_data);
            return;
        }
        _data = this.cleanBuffer(_data);
        for (let i = 0; i < _data.length; i+=1) {
            if (_data[i] !== '') {
                this.handleRawData(_data[i]);
            }
        }
    }

    isBuffered(data) {
        return data.slice(-1) !== '\n';
    }

    buffer(data) {
        if (this.bufferedMessage === null) {
            this.bufferedMessage = '';
        }
        this.bufferedMessage += data;
    }

    cleanBuffer(data) {
        let _data = data;
        if (this.bufferedMessage) {
            _data = this.bufferedMessage + data;
            this.bufferedMessage = null;
        }
        return _data.split('\r\n');
    }

    /**
     * Handles the 'error' event (when the socket cannot handle something and have to stop running) of the IRC Socket. Allows to do hooks once the event is triggered
     * @param {Object} err the error we received
     * @returns {void} nothing
     * @version 1.0
     */
    onError(err) {
        console.error(chalk.magenta(new Date().toUTCString()), Object.keys(this.channels).join(';'), ' MHK-IRC/Error: Socket error ', err.code, err.toString());
        this.connected = false;
        if (err.code === 'ETIMEDOUT') {
            this.onTimeout();
        }
    }

    /**
     * Handles the 'close' event (when the socket closes itself, by user demand or crash/timeout) of the IRC Socket. Allows to do hooks once the event is triggered
     * @returns {void} nothing
     * @version 1.0
     */
    onClose() {
        this.reset();
        this.socket.destroy();
        if (this.reconnect) {
            this.enqueuedCapabilities = [...this.requiredCapabilities];
            setTimeout(() => {
                this.connect(TwitchHelper.serverPort, TwitchHelper.serverAddress)
            }, 5000);
        }
    }

    reset() {
        this.connected = false;
        this.authenticated = false;
        this.enqueuedCapabilities = [...this.requiredCapabilities];
        this.capabilities.successful = [];
        this.capabilities.failed = [];
    }

    /**
     * Sends a PONG message to the server
     * @param {string} pingReq the ping request from the server
     * @returns {void} nothing
     * @version 1.0
     */
    pong(pingReq) {
        const regex = /^PING :(\S+)/i;
        if (regex.test(pingReq)) {
            const matched = regex.exec(pingReq);
            const serverName = matched[1];
            this.raw('PONG :' + serverName);
        }
    }

    ping() {
        this.raw('PING');
    }

    /**
     * Enables a continuous verification, every minute, of the good health of the socket.
     * Allows to handle events such as a disconnection (which stop the checks), or if the server havent asked any PING in a while.
     * @returns {void} nothing
     * @version 1.0
     */
    verifyPingability() {
        setTimeout(() => {
            if (!this.connected) { return false; }
            // Checking PINGS
            const intervalPing = 1000 * 30;
            if (this.socket !== null && Date.now() > this.activity.ping + intervalPing) {
                this.ping();
            }
            // Checking NO answer
            const maxDelayTimeout = 1000 * 60 * 5;
            if (this.socket !== null && Date.now() > this.activity.last_answer + maxDelayTimeout) {
                this.disconnect();
                this.reconnect = true;
                this.createSocket();
                this.connect();
            }
            this.verifyPingability();
            return true;
        }, 1 * 60 * 1000);
    }

    /**
     * defines the Channel to be used while the IRC connection is active
     * @param {Channel} channel the Channel instance to link to the IRC client
     * @returns {void} nothing
     * @version 1.0
     */
    addChannel(channel) {
        const channelName = this.getChannelName(channel._name);
        this.channels[channelName] = channel;
        if (this.connected) {
            this.join(`#${channelName}`);
        }
    }

    /**
     * provides the channel used by the IRC connection.
     * @param {name} name the channel name, with or without the #
     * @returns {Channel} the related channel
     * @version 1.0
     */
    getChannel(name) {
        const channelName = this.getChannelName(name);
        return this.channels[channelName] || null;
    }

    getChannelName(name) {
        return name.startsWith('#') ? name.substring(1) : name;
    }

    getChannelTag(name) {
        return name.startsWith('#') ? name : '#'+name;
    }

    /**
     * defines the username to use when connecting to the IRC server
     * @param {String} username the username to use
     * @returns {void} nothing
     * @version 1.0
     */
    setNick(username) {
        this.username = username;
    }

    /**
     * provide the NICK used to connect to the IRC server
     * @returns {String} the username for the server
     * @version 1.0
     */
    getNick() {
        return this.username || env('TCH_USERNAME');
    }

    /**
     * defines the password to use when connecting to the IRC Server, in combo with NICK.
     * @param {String} password the password used for connection
     * @returns {void} nothing
     * @version 1.0
     */
    setPass(password) {
        this.password = password;
    }

    /**
     * provides the IRC client password. Usually for Twitch, it is an oauth token.
     * @returns {String} the password used to connect to the IRC server
     */
    getPass() {
        return this.password || env('TCH_PASSWORD');
    }

    /**
     * Sets the configured attribute of the Irc object to val. Not used anymore
     * @param {Boolean} val the value or expression test to check if it is configured
     * @returns {void} nothing
     * @deprecated since version 0.3, it is no longer used
     * @version 1.0
     */
    setConfigured(val) {
        this.config.health.configured = val;
    }

    setHostname(val) {
        this.host = val;
    }

    /**
     * Disconnects the socket from the IRC server gracefully, disabling the reconnect setting
     * @returns {void} nothing
     * @version 1.0
     */
    disconnect() {
        this.reconnect = false;
        if (this.socket !== null && !this.socket.destroyed) {
            this.socket.destroy();
        }
    }

    /**
     * Connects the Irc object to the server. If no username, password or host/port is defined, it will fail.
     * @returns {void} nothing
     * @version 1.0
     */
    connect() {
        if (this.socket === null || this.socket.destroyed) {
            this.createSocket();
        }
        this.socket.connect(TwitchHelper.serverPort, TwitchHelper.serverAddress);
    }

    /**
     * Authenticates with the IRC Server, taking in charge the quota limit.
     * @returns {void} nothing
     * @version 1.0
     */
    authenticate() {
        if (this.connected && !this.authenticated) {
            if (this.getPass() !== null) {
                // User has a password
                this.sendNickPass();
            } else if (this.getPass() === null) {
                // User has no password
                this.sendNick();
            } else {
                // Repeat same method until authenticated, every second
                setTimeout(() => { this.authenticate(); }, 1000 * 1);
            }
        }

        if (this.connected === false) {
            console.log(chalk.magenta(new Date().toUTCString()), Object.keys(this.channels).join(';'), chalk.yellow('MHK-IRC/Authenticate'), chalk.red('Unable to authenticate as the client is not connected. Aborting.'));
        }
        return true;
    }

    sendNickPass() {
        this.raw('PASS ' + this.getPass());
        this.raw('NICK ' + this.getNick());
    }

    sendNick() {
        this.raw('NICK ' + this.getNick());
    }

    /**
     * Requests to the IRC Server the capacities to see joins and parts, the commands typed and the user tags for management.
     * @returns {void} nothing
     * @version 1.0
     */
    requestCapacities() {
        if (this.connected && this.authenticated) {
            if (this.enqueuedCapabilities.length > 0) {
                const capName = this.enqueuedCapabilities.shift();
                this.raw('CAP REQ :' + capName);
                setTimeout(() => { this.requestCapacities(); }, 100 * 1);
            } else if (this.enqueuedCapabilities.length === 0) {
                return true;
            } else {
                // Retrying in one second
                setTimeout(() => { this.requestCapacities(); }, 1000 * 1);
            }
        } else {
            // Authentication wasn't successful, aborting
            console.error('authentication failed.', Object.keys(this.channels).join(';'))
        }
        return true;
    }
}