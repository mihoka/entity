const ApiCall = require('./ApiCall');
const Command = require('./Command');
const Channel = require('./Channel');
const Irc = require('./Irc');

module.exports = {
  Api: ApiCall,
  Command,
  Channel,
  Irc,
}