const http = require('http');
const https = require('https');

module.exports = class ApiCall {
  // eslint-disable-next-line max-params
  constructor(hostname, endpoint, method, port, secured, headers, body) {
    this.endpoint = endpoint || '/';
    this.body = body || null;
    this.hostname = hostname || null;
    if (this.hostname === null) {
      throw new Error('No hostname was defined for the API Call');
    }
    this.method = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'UPDATE'].indexOf(method) > -1 ? method : 'GET';
    this.port = typeof port === 'number' ? port : 443;
    this.secured = typeof secured === 'boolean' ? secured : true;
    this.headers = typeof headers === 'object' ? headers : [];
  }
  setHeader(key, value) {
    this.headers[key] = value;
  }
  setHeaders(array) {
    if (!Array.isArray(array)) {
      return false;
    }
    this.headers = array;
    return true;
  }
  getReturnedValue() {
    return this.result;
  }
  getStatusCode() {
    return this.result.status;
  }
  getResponseBody() {
    return this.result.body;
  }
  getResponseHeaders() {
    return this.result.headers;
  }
  execute() {
    return new Promise((resolve, reject) => {
      const options = {
        port: this.port,
        hostname: this.hostname,
        path: this.endpoint,
        method: this.method,
        headers: this.headers,
      };
      const request = (this.secured ? https : http).request(options, (res) => {
        // Requesting the content
        const rsp = {
          raw: '',
          status: res.statusCode,
          headers: res.headers,
        };

        res.setEncoding('utf8');
        res.on('data', (chunk) => {
          // Once receiving data, we buffer it
          rsp.raw += chunk;
        });

        res.on('end', () => {
          // Once everything is obtained, we process it
          try {
            rsp.body = {
              data: JSON.parse(rsp.raw),
              status: 'success',
            };
            resolve(rsp);
          } catch (e) {
            rsp.body = {
              status: 'failed',
              message: e,
            };
            reject(rsp);
          }
        });
      });
      request.on('error', (err) => {
        return reject({ status: 'failed', message: err });
      });
      request.on('timeout', () => {
        return reject({ status: 'failed', message: 'Request timed out.' });
      });
      request.setTimeout(2000);
      if (this.method !== 'GET' && this.body) {
        request.write(this.body);
      }
      request.end();
    });
  }
}