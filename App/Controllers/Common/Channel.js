/* eslint-disable max-lines */
const Irc = require('App/Controllers/Common/Irc');
const ApiMihoka = require('App/Controllers/Mihoka/Api');
const BuiltIns = require('App/Helpers/BuiltIns');
const env = require('App/Helpers/Env');
const GiveAway = require('App/Classes/Giveaway');

module.exports = class Channel {
  constructor() {
    // Stores the Id of the channel on a specified service, such as Twitch or YouTube. String
    this._id = null;
    // Database ID of the account
    this._uid = null;
    // Name of the channel, as a string
    this._name = null;
    // Client communication to the channel. Socket
    this._client = null;
    // Channel settings. Object
    this._settings = {};
    // Last time the client was updated. Long
    this._updated = 0;
    this._hasUpdates = false;
    this._commands = [];
    this._queue = [];
    this._ircUser = {};
  }
  get name() {
    return this._name;
  }
  get id() {
    return this._id;
  }
  fillPlaceholders(message) {
    return message;
  }
  fromEnv() {
    const channelId = env('CHANNEL_ID');
    if (channelId !== null) {
      this._uid = channelId;
      this.fetchChannel();
    }
  }
  fetchChannel() {
    const that = this;
    if (!this._id) {
      const fetchResult = ApiMihoka.getChannel(this._uid);
      fetchResult.then((result) => {
        that._commands = JSON.parse(result.commands) || [];
        that._settings = JSON.parse(result.settings) || {};
        that._giveaways = JSON.parse(result.giveaways) || {};
        that._giveaways.current = GiveAway.fromObject(that._giveaways.current);
        that._id = result.roomId;
        that._name = result.roomName;
        that._uid = result.uuid;
        that._natives = new BuiltIns();
        that.init();
      }).catch((reason) => {
        const _errMsg = 'Failed to fetch user informations from APIs.';
        console.log(_errMsg, reason);
      });
    }
  }
  triggerUpdate() {
    this._hasUpdates = true;
  }
  init() {
    if (process.env.DRY_RUN) {
      return;
    }
    this._client = new Irc();
    console.log(`adding ${this._name}'s IRC client to the stuff to start!`);
    this._client.addChannel(this);
    this._client.connect();
  }
  poll() {
    const that = this;
    if (this._hasUpdates && this._updated + 30000 < Date.now()) {
      this.purgeOldActivity();
      this.update();
    } else if (this._updated + 1800000 < Date.now()) {
      // 30min since last update, updating
      this.purgeOldActivity();
      this.update();
    }
    if (this._queue.length > 0) {
      const message = this._queue.shift();
      this.send(message);
    }
    if (this.isAnnounceEnabled()) {
      // Auto-announces polling system
      if (this._settings.announces.list.length > 0 && this.isAnnounceTriggered()) {
        const nextId = this._settings.announces.list.length -1 < this._settings.announces.next ? 0 : this._settings.announces.next;
        this._settings.announces.next = nextId + 1;
        this._settings.announces.lastCalled = Date.now();
        this._settings.announces.messageCountSinceCalled = 0;
        this.enqueue(this._settings.announces.list[nextId]);
      }
    }
    setTimeout(() => { that.poll(); }, that._ircUser && (that._ircUser.moderator || that._ircUser.broadcaster) ? 1000 : 2000);
  }
  send(message) {
    this._client.sendMessage(this._name, message);
  }
  enqueue(message) {
    this._queue.push(message);
  }
  update() {
    // sends the updated changes to the API Server, once we get the API Client working
    const updateResult = ApiMihoka.updateChannel(this._uid, this);
    updateResult.then(() => {
      this._hasUpdates = false;
    }).catch((reason) => {
      const _errMsg = `CHANNEL::update() - Failed to update channel ${this._name}: ${JSON.stringify(reason)}`;
      console.log(_errMsg);
    });
    this._updated = Date.now();
  }
  isAnnounceEnabled() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.announces) { this._settings.announces = {}; }
    if (!this._settings.announces.lastCalled) { this._settings.announces.lastCalled = 0; }
    if (!this._settings.announces.messageCountSinceCalled) { this._settings.announces.messageCountSinceCalled = 0; }
    if (!this._settings.announces.list) { this._settings.announces.list = []; }
    if (!this._settings.announces.mode) { this._settings.announces.mode = 3; }
    if (!this._settings.announces.enabled) { this._settings.announces.enabled = false; }
    if (!this._settings.announces.rules) { this._settings.announces.rules = {}; }
    if (!this._settings.announces.rules.messages) { this._settings.announces.rules.messages = 10; }
    if (!this._settings.announces.rules.minutes) { this._settings.announces.rules.minutes = 5; }
    if (!this._settings.announces.next) { this._settings.announces.next = 0; }
    return this._settings.announces.enabled;
  }
  createAverageNode() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.average) { this._settings.average = {}; }
    if (!Reflect.has(this._settings.average, 'recording')) { this._settings.average.recording = false; }
    if (!Reflect.has(this._settings.average, 'data')) { this._settings.average.data = {}; }
  }
  isAnnounceTriggered() {
    if (!this._settings.announces.enabled) {
      return false;
    }

    if ((this._settings.announces.mode === 2 || this._settings.announces.mode === 3) && this._settings.announces.rules.messages > this._settings.announces.messageCountSinceCalled) {
      // Message count is not good yet
      return false;
    }

    const minutesInEpoch = this._settings.announces.rules.minutes*60*1000;
    if ((this._settings.announces.mode === 1 || this._settings.announces.mode === 3) && minutesInEpoch + this._settings.announces.lastCalled > Date.now()) {
      // Time is not good yet
      return false;
    }
    return true;
  }
  isAutoquoteTriggered() {
    if (!this._settings.autoquotes.enabled) {
      return false;
    }
    if ((this._settings.autoquotes.mode === 2 || this._settings.autoquotes.mode === 3) && this._settings.autoquotes.rules.messages > this._settings.autoquotes.messageCountSinceCalled) {
      // Message count is not good yet
      return false;
    }
    const minutesInEpoch = this._settings.autoquotes.rules.minutes*60*1000;
    if ((this._settings.autoquotes.mode === 1 || this._settings.autoquotes.mode === 3) && minutesInEpoch + this._settings.autoquotes.lastCalled > Date.now()) {
      // Time is not good yet
      return false;
    }
    return true;
  }
  buildAliasNode() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.aliases) { this._settings.aliases = {}; }
    return true;
  }
  getActivity(userId) {
    this.buildActivityNode();
    if (!this._settings.activity.list[userId]) { this._settings.activity.list[userId] = 0; }
    return this._settings.activity.list[userId];
  }
  purgeOldActivity() {
    this.buildActivityNode();
    const keys = Object.keys(this._settings.activity.list);
    const welcomeDelay = this.getWelcomeDelay() * 60000;
    keys.forEach((keyName) => {
      if (Reflect.get(this._settings.activity.list, keyName) < Date.now() - welcomeDelay) {
        Reflect.deleteProperty(this._settings.activity.list, keyName);
      }
    })
  }
  getWelcomeDelay() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.welcome) { this._settings.welcome = {}; }
    if (!this._settings.welcome.delay) { this._settings.welcome.delay = 480; }
    return this._settings.welcome.delay;
  }
  setWelcomeDelay(delay) {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.welcome) { this._settings.welcome = {}; }
    if (!this._settings.welcome.delay) { this._settings.welcome.delay = 480; }
    this._settings.welcome.delay = delay;
  }
  buildActivityNode() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.activity) { this._settings.activity = {}; }
    if (!this._settings.activity.list) { this._settings.activity.list = {}; }
  }
  setActivity(userId) {
    this.buildActivityNode();
    this._settings.activity.list[userId] = Date.now();
  }
  buildWelcomeNode() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.welcome) { this._settings.welcome = {}; }
    if (!this._settings.welcome.mode) { this._settings.welcome.mode = 0; }
    if (!this._settings.welcome.users) { this._settings.welcome.users = {}; }
    if (!this._settings.welcome.message) { this._settings.welcome.message = null; }
  }
  buildVipNode() {
    if (!this._settings.vip) { this._settings.vip = {}; }
    if (!this._settings.vip.list) { this._settings.vip.list = []; }
    if (!this._settings.vip.message) { this._settings.vip.message = null; }
  }
  buildAutoQuoteNode() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.autoquotes) { this._settings.autoquotes = {}; }
    if (!this._settings.autoquotes.enabled) { this._settings.autoquotes.enabled = false; }
    if (!this._settings.autoquotes.rules) { this._settings.autoquotes.rules = {}; }
    if (!this._settings.autoquotes.rules.minutes) { this._settings.autoquotes.rules.minutes = 60; }
    if (!this._settings.autoquotes.rules.messages) { this._settings.autoquotes.rules.messages = 10; }
    if (!this._settings.autoquotes.rules.messageCountSinceCalled) { this._settings.autoquotes.rules.messageCountSinceCalled = 10; }
  }
  buildQuoteNode() {
    if (!this._settings) { this._settings = {}; }
    if (!this._settings.quotes) { this._settings.quotes = []; }
    this.buildAutoQuoteNode();
  }
  buildGiveawayNode() {
    if (!this._giveaways) { this._giveaways = {}; }
    if (!this._giveaways.current) { this._giveaways.current = null; }
    if (!this._giveaways.previous) { this._giveaways.previous = []; }
  }
  canBeGreeted(userId) {
    this.buildWelcomeNode();
    if (this._settings.welcome.mode === '0') {
      return false;
    }
    if (this._settings.welcome.mode === '2') {
      return this._settings.vip.list.indexOf(userId) > -1;
    }
    return true;
  }
  getWelcome(userId) {
    this.buildWelcomeNode();
    if (this._settings.welcome.users[userId]) {
      // per user message
      return this._settings.welcome.users[userId];
    }
    this.buildVipNode();
    if (this._settings.vip.list[userId]) {
      // vip message
      return this._settings.vip.message;
    }
    // normal message
    return this._settings.welcome.message;
  }
  onUserState(message) {
    this._ircUser = message.user;
  }
  handleSub(message) {
    const subTotalTime = message.tags['msg-param-cumulative-months'] || message.tags['msg-param-months'] || 1;
    const subDuration = message.tags['msg-params-gift-months'] || 1;
    const subCode = message.tags['msg-param-sub-plan'];
    const subTarget = message.tags['msg-param-recipient-display-name'] || message.tags['msg-param-recipient-user-name'];

    const genericMessages = {
      sub: `Merci %%user%% pour l'abonnement avec le Tier %%tier%%!`,
      resub: `Merci %%user%% pour le réabonnement de %%months%% mois avec le Tier %%tier%%!`,
      subgift: `Merci %%user%% pour le cadeau d'abonnement de %%giftedmonths%% mois à %%target%% avec le Tier %%tier%%!`,
      anonsubgift: `Merci au donateur anonyme pour le sub à %%user%% de %%giftedmonths%% mois avec un tier %%tier%%!`,
    };

    const tiers = [
      {
        internal: 'sub1',
        code: '1000',
        tier: '1',
      },
      {
        internal: 'sub2',
        code: '2000',
        tier: '2',
      },
      {
        internal: 'sub3',
        code: '3000',
        tier: '3',
      },
      {
        internal: 'subprime',
        code: 'Prime',
        tier: '1 (Prime)',
      },
    ];

    const matchingTier = tiers.find((t) => t.code === subCode);
    if (!matchingTier || !Object.keys(genericMessages).includes(message.tags['msg-id'])) {
      // invalid msg-id, we skip
      return;
    }
    if (['subgift', 'anonsubgift'].includes(message.tags['msg-id'])) {
      // eslint-disable-next-line no-warning-comments
      // TODO: enqueue another message if gift
      const subGiftMessage = (this._settings[BuiltIns.translationKeys.gift] || genericMessages[message.tags['msg-id']]).
      replace(/%%user%%/gi, message.user.display).
      replace(/%%months%%/gi, subTotalTime).
      replace(/%%giftedmonths%%/gi, subDuration).
      replace(/%%tier%%/gi, matchingTier.tier).
      replace(/%%target%%/gi, subTarget);
      this.enqueue(subGiftMessage);
    } else {
      const matchingTierMessage = this._settings[BuiltIns.translationKeys[matchingTier.internal]];
      const matchingGenericMessage = this._settings[BuiltIns.translationKeys.sub];
      const announcedSub = (matchingTierMessage || matchingGenericMessage || genericMessages[message.tags['msg-id']]).
        replace(/%%user%%/gi, subTarget || message.user.display).
        replace(/%%months%%/gi, subTotalTime).
        replace(/%%giftedmonths%%/gi, subDuration).
        replace(/%%tier%%/gi, matchingTier.tier).
        replace(/%%target%%/gi, subTarget);
      this.enqueue(announcedSub);
    }
  }
  handleRaid(message) {
    const raidersCount = message.tags['msg-param-viewerCount'];
    const raiderName = message.tags['msg-param-displayName'];
    const genericRaidMessage = `%%user%% nous raid avec %%count%% spectateurs ! Défendez le chat!`;
    const definedRaidMessage = this._settings[BuiltIns.translationKeys.raid];
    console.log('found raid message:', definedRaidMessage);

    const announcedRaid = (definedRaidMessage || genericRaidMessage).
      replace(/%%user%%/gi, raiderName).
      replace(/%%count%%/gi, raidersCount);
    this.enqueue(announcedRaid);
  }
  handleMessage(message) {
    this.isAnnounceEnabled();
    this._settings.announces.messageCountSinceCalled += 1;

    if (!message.user.moderator && !message.user.broadcaster) {
      if (this.getActivity(message.user.id) + 1000 >= Date.now()) {
        return false;
      }
    }

    this.createAverageNode();
    if (this._settings.average.recording) {
      // we will check the message if theres a notation for us
      const rgx = /^\d+?/i;
      if (rgx.test(message.body)) {
        const hasKey = Reflect.has(this._settings.average.data, message.user.id);
        const value = parseFloat(message.body);
        if (!hasKey && !isNaN(value) && value >= 0 && value <= 10) {
          Reflect.set(this._settings.average.data, message.user.id, value);
        }
      }
    }

    if (this.canBeGreeted(message.user.id)) {
      const activityDelay = this.getWelcomeDelay() * 60000;
      if (this.getActivity(message.user.id) + activityDelay < Date.now()) {
        const foundCustomGreeting = this.getWelcome(message.user.id);
        if (foundCustomGreeting !== null) {
          this.enqueue(foundCustomGreeting.replace('%s', message.user.display));
        }
      }
    }
    this.setActivity(message.user.id);

    const that = this;
    if (this._natives.invoke(message, that)) {
      return true;
    }
    this._commands.find((command, index) => {
      const hasSpace = message.body.length > command.input.length ? ' ' : '';
      if (message.body.toLowerCase().startsWith(`${command.input.toLowerCase()}${hasSpace}`)) {
        const commandDelay = command.delay * 1000;
        const delayValid = typeof command.called !== 'undefined' && command.called + commandDelay < Date.now();
        if (command.ignoresDelay || delayValid) {
          this._commands[index].called = Date.now();
          that.enqueue(command.output.replace('%s', message.user.display));
        }
        return true;
      }
      return false;
    });
    // -- custom Elkinoo commands
    if (that._name === 'elkinoo') {
      const customCmdJeux = BuiltIns.cmdCustomElkinooJeux();
      if (customCmdJeux.triggeredBy(message)) {
        customCmdJeux.execute(message, {
          command: customCmdJeux,
          channel: that,
        });
        return true;
      }
    }
    // -- end of custom Elkinoo commands
    return message;
  }
}