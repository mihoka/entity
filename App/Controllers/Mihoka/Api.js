const ApiCall = require('App/Controllers/Common/ApiCall');
const MihokaConfig = require('App/Config/Api').Mihoka;
module.exports = class ApiMihoka {
    static getChannels () {
        return new Promise((resolve, reject) => {
            new ApiCall(
                MihokaConfig.hostname,
                process.env.APP_DEBUG === 'on' ? '/devchannels' : '/channels',
                'GET',
                31625,
                false,
                {}
            ).execute().
            then((response) => {
                if (response.body.status !== 'success') {
                    reject(response);
                }
                if (response.status !== 200) {
                    reject(response);
                }
                if (!response.body.data) {
                    reject(response);
                }
                resolve(response.body.data);
            }).
            catch((reason) => {
                reject(reason);
            });
        });
    }

    static getChannel(id) {
        return new Promise((resolve, reject) => {
            const call = new ApiCall(MihokaConfig.hostname, '/channels/'+id, 'GET', MihokaConfig.hostport, MihokaConfig.hostport === 443, { Authorization: MihokaConfig.bearerId }).execute();
            call.then((response) => {
                if (response.body.status !== 'success') {
                    reject(response.message);
                }
                if (response.status !== 200) {
                    reject(response.message);
                }
                if (!response.body.data) {
                    reject(response.message);
                }
                resolve(response.body.data);
            }).
            catch((reason) => {
                reject(reason.message);
            });
        });
    }

    static updateChannel(id, data) {
        const channelBody = {
            settings: data._settings,
            commands: data._commands,
            giveaways: data._giveaways,
            roomId: data._id,
            roomName: data._name,
        }
        return new Promise((resolve, reject) => {
            const call = new ApiCall(MihokaConfig.hostname, '/channels/'+ id, 'PUT', MihokaConfig.hostport, MihokaConfig.hostport === 443, { Authorization: MihokaConfig.bearerId, 'Content-Type': 'application/json' }, JSON.stringify(channelBody)).execute();
            call.then((response) => {
                if (response.status !== 200) {
                    reject(response.body);
                }
                if (!response.body.data) {
                    reject(response.body);
                }
                resolve(response.body.data);
            }).
            catch((reason) => {
                reject(reason.message);
            });
        });
    }
}