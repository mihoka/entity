const Twitch = require('./Twitch/All');
const Common = require('./Common/All');
const Mihoka = require('./Mihoka/All');
const DailyMotion = require('./Dailymotion/All');


module.exports = {
  Twitch,
  Common,
  Mihoka,
  DailyMotion,
}