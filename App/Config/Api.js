const env = require('App/Helpers/Env');
module.exports = {
    Mihoka: {
        hostname: env('SPE_API_HOST') || 'mihoka-v2-api',
        hostport: Number(env('SPE_API_PORT')) || 31625,
        clientId: env('SPE_CID') || '',
        secretId: env('SPE_CSK') || '',
        bearerId: env('SPE_TOK') || 'Bearer e43831ad-3f76-46c8-ab45-aa5da9a0a8b1',
    },
    Twitch: {
        hostname: env('TCH_API') || 'api.twitch.tv',
        clientId: env('TCH_CID') || '',
        secretId: env('TCH_CSK') || '',
        bearerId: env('TCH_TOK') || '',
    }
}