FROM node:lts-alpine

WORKDIR /app

COPY . .
RUN rm -rf node_modules
RUN rm -rf .git

RUN npm ci --only=production
CMD npm start

# Problem: spawning a child removes his context about the ENV
# We need to find a fix for that